<?php

use Nunzion\Debug\CurrentDumper;
use Nunzion\Debug\StaticResourceProvider;
use Nunzion\StackTrace\StackTrace;

StaticResourceProvider::startup();
CurrentDumper::init();

/**
 * Print a value as dump.
 * @param mixed $var            The value to dump.
 * @param bool  $die            True to stop after printing.
 * @param bool  $showStackTrace True to show where this function was called.
 * @return mixed The value (if die == false).
 */
function dump($var, $die = false, $showStackTrace = true)
{
    CurrentDumper::dump($var, $showStackTrace);
    if ($die)
        die;

    return $var;
}

/**
 * Print the current stacktrace.
 * @param bool $die True to stop after printing.
 */
function trace($die = false)
{
    CurrentDumper::dump(CurrentDumper::getStackTraceFilterInstance()->filter(StackTrace::getCurrent(true)), false);
    if ($die)
        die;
}
