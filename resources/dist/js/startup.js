var requireNunzionPhpDumer = require.config({
    baseUrl: nunzionPhpDumperBasePath + "/dist/js",
    context: 'nunzion-php-dumper',
    paths: {
        "jquery": "../../lib/jquery/jquery.min"
    }
});

requireNunzionPhpDumer(["dumper"], function () { });
