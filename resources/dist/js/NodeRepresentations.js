///<reference path="definitions.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "ObjectHelper", "Template", "Representations", "TabRepresentations"], function (require, exports, ObjectHelper, Template, Representations, TabRepresentations) {
    //#region Abstract Representations
    var CallFrameNodeRepresentation = (function (_super) {
        __extends(CallFrameNodeRepresentation, _super);
        function CallFrameNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("callFrame");
            this.type = "CallFrameNode";
        }
        return CallFrameNodeRepresentation;
    })(TabRepresentations.TabbedNodeRepresentation);
    exports.CallFrameNodeRepresentation = CallFrameNodeRepresentation;
    //#endregion
    //#region Simple Representations
    var AbortDepthNodeRepresentation = (function (_super) {
        __extends(AbortDepthNodeRepresentation, _super);
        function AbortDepthNodeRepresentation(node, parentElement) {
            _super.call(this, node, parentElement);
            // get and render template
            var tmpl = Template.get("AbortDepth");
            var data = { node: node };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.addClass("abortDepth");
            parent.html(html);
            Representations.trimHtml(parentElement);
        }
        return AbortDepthNodeRepresentation;
    })(Representations.NodeRepresentation);
    exports.AbortDepthNodeRepresentation = AbortDepthNodeRepresentation;
    var UnknownNodeRepresentation = (function (_super) {
        __extends(UnknownNodeRepresentation, _super);
        function UnknownNodeRepresentation(node, parentElement) {
            _super.call(this, node, parentElement);
            // get and render template
            var tmpl = Template.get("Unknown");
            var data = { node: node };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.addClass("unknown");
            parent.html(html);
            Representations.trimHtml(parentElement);
        }
        return UnknownNodeRepresentation;
    })(Representations.NodeRepresentation);
    exports.UnknownNodeRepresentation = UnknownNodeRepresentation;
    var PrimitiveNodeRepresentation = (function (_super) {
        __extends(PrimitiveNodeRepresentation, _super);
        function PrimitiveNodeRepresentation(node, parentElement) {
            _super.call(this, node, parentElement);
            // get and render template
            var tmpl = Template.get("Primitive");
            var data = { node: node, shortForm: PrimitiveNodeRepresentation.shortForm };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.addClass("primitive");
            parent.html(html);
            Representations.trimHtml(parentElement);
            Representations.trimHtml(parent.children("span.content").get(0));
        }
        Object.defineProperty(PrimitiveNodeRepresentation, "shortForm", {
            get: function () { return true; },
            enumerable: true,
            configurable: true
        });
        return PrimitiveNodeRepresentation;
    })(Representations.NodeRepresentation);
    exports.PrimitiveNodeRepresentation = PrimitiveNodeRepresentation;
    //#endregion
    //#region Complex Representations
    var ArrayNodeRepresentation = (function (_super) {
        __extends(ArrayNodeRepresentation, _super);
        function ArrayNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("array");
            this.type = "ArrayNode";
        }
        return ArrayNodeRepresentation;
    })(TabRepresentations.TabbedNodeRepresentation);
    exports.ArrayNodeRepresentation = ArrayNodeRepresentation;
    var ObjectNodeRepresentation = (function (_super) {
        __extends(ObjectNodeRepresentation, _super);
        function ObjectNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("object");
            this.type = "ObjectNode";
        }
        return ObjectNodeRepresentation;
    })(TabRepresentations.TabbedNodeRepresentation);
    exports.ObjectNodeRepresentation = ObjectNodeRepresentation;
    //#region TraceValue Representations
    var StackTraceNodeRepresentation = (function (_super) {
        __extends(StackTraceNodeRepresentation, _super);
        function StackTraceNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            _super.call(this, node, parentElement, representationStorage);
            this.elementReferences = elementReferences;
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.elements = [];
            // get and render template
            var tmpl = Template.get("StackTrace");
            var data = { elements: node.getElements().slice(1) }; // Do not show the first
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.addClass("stackTrace");
            parent.html(html);
            Representations.trimHtml(parentElement);
            var titles = parent.children("dl").children("dt");
            titles.each(function (i, el) { return Representations.trimHtml(el); });
            var definitions = parent.children("dl").children("dd");
            definitions.empty();
            for (var key in data.elements) {
                key = +key; // convert to number
                var element = data.elements[key];
                var nav = titles.eq(key).children("nav").eq(0);
                nav.click({ representation: this, id: key + 1 }, Representations.switchStateClick);
                nav.dblclick({ representation: this, id: key + 1 }, Representations.resetStateClick);
                this.elements[key + 1] = {
                    title: titles.eq(key).children("outline").eq(0), definition: definitions.eq(key),
                    nav: nav, open: false,
                    representation: null
                };
            }
        }
        StackTraceNodeRepresentation.prototype.open = function (id) {
            if ((typeof id !== "number") || (this.elements[id] === undefined)) {
                return null;
            }
            else {
                this.elements[id].nav.addClass("open");
                if (this.elements[id].representation === null) {
                    Representations.animateHide(this.elements[id].definition, true, true);
                    var node = this.node.getElements()[id];
                    var newReferences = ObjectHelper.clone(this.elementReferences);
                    newReferences[node.getId()] = this.elements[id].title.add(this.elements[id].definition);
                    this.elements[id].representation = this.nodeConverterRegistry.convert(node, this.elements[id].definition[0], newReferences, this.representationStorage);
                }
                if (!this.elements[id].open) {
                    Representations.animateShow(this.elements[id].definition);
                    this.elements[id].open = true;
                }
                return this.elements[id].representation;
            }
        };
        StackTraceNodeRepresentation.prototype.close = function (id, immidiate) {
            if (immidiate === void 0) { immidiate = false; }
            if ((typeof id !== "number") || (typeof this.elements[id] === "undefined")) {
                return false;
            }
            else {
                this.elements[id].nav.removeClass("open");
                Representations.animateHide(this.elements[id].definition, immidiate);
                this.elements[id].open = false;
                return true;
            }
        };
        StackTraceNodeRepresentation.prototype.switch = function (id) {
            if ((typeof id !== "number") || (typeof this.elements[id] === "undefined")) {
                return;
            }
            else {
                if (!this.elements[id].open)
                    this.open(id);
                else
                    this.close(id);
            }
        };
        StackTraceNodeRepresentation.prototype.reset = function (id) {
            if ((typeof id === "number") && (typeof this.elements[id] !== "undefined")) {
                this.elements[id].nav.removeClass("open");
                Representations.animateHide(this.elements[id].definition, false, true);
                this.elements[id].representation = null;
                this.elements[id].open = false;
            }
        };
        StackTraceNodeRepresentation.prototype.getState = function () {
            var _this = this;
            var elements = this.elements.map(function (element, id) {
                if (element.representation === null)
                    return null;
                var node = _this.node.getElements()[id];
                var state = null;
                if (element.representation !== null)
                    state = element.representation.getState();
                return { id: id, open: element.open, nodeType: node.getNodeType(), criteria: node.getCriteriaString(), state: state };
            });
            return { type: "StackTrace", elements: elements };
        };
        StackTraceNodeRepresentation.prototype.setState = function (state) {
            var _this = this;
            if ((state.type !== "StackTrace") || (this.elements.length !== state.elements.length)) {
                this.elements.forEach(function (element, id) { return _this.reset(id); });
            }
            else {
                // change state
                this.elements.forEach(function (element, id) {
                    var node = _this.node.getElements()[id];
                    if ((typeof state.elements[id] !== "undefined") && (state.elements[id] !== null) &&
                        (state.elements[id].nodeType === node.getNodeType()) && (state.elements[id].criteria === node.getCriteriaString())) {
                        _this.open(id);
                        if (!state.elements[id].open)
                            _this.close(id, true);
                        if ((element.representation !== null) && (state.elements[id].state !== null))
                            element.representation.setState(state.elements[id].state);
                    }
                    else {
                        _this.reset(id);
                    }
                });
            }
        };
        return StackTraceNodeRepresentation;
    })(Representations.ComplexNodeRepresentation);
    exports.StackTraceNodeRepresentation = StackTraceNodeRepresentation;
    var FunctionCallFrameNodeRepresentation = (function (_super) {
        __extends(FunctionCallFrameNodeRepresentation, _super);
        function FunctionCallFrameNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("functionCallFrame");
            this.type = "FunctionCallFrameNode";
        }
        return FunctionCallFrameNodeRepresentation;
    })(CallFrameNodeRepresentation);
    exports.FunctionCallFrameNodeRepresentation = FunctionCallFrameNodeRepresentation;
    var ClosureCallFrameNodeRepresentation = (function (_super) {
        __extends(ClosureCallFrameNodeRepresentation, _super);
        function ClosureCallFrameNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("closureCallFrame");
            this.type = "ClosureCallFrameNode";
        }
        return ClosureCallFrameNodeRepresentation;
    })(CallFrameNodeRepresentation);
    exports.ClosureCallFrameNodeRepresentation = ClosureCallFrameNodeRepresentation;
    var StaticMethodCallFrameNodeRepresentation = (function (_super) {
        __extends(StaticMethodCallFrameNodeRepresentation, _super);
        function StaticMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("staticMethodCallFrame");
            this.type = "StaticMethodCallFrameNode";
        }
        return StaticMethodCallFrameNodeRepresentation;
    })(CallFrameNodeRepresentation);
    exports.StaticMethodCallFrameNodeRepresentation = StaticMethodCallFrameNodeRepresentation;
    var InstanceMethodCallFrameNodeRepresentation = (function (_super) {
        __extends(InstanceMethodCallFrameNodeRepresentation, _super);
        function InstanceMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, elementReferences, nodeConverterRegistry, representationStorage);
            var parent = $(parentElement);
            parent.addClass("instanceMethodCallFrame");
            this.type = "InstanceMethodCallFrameNode";
        }
        return InstanceMethodCallFrameNodeRepresentation;
    })(CallFrameNodeRepresentation);
    exports.InstanceMethodCallFrameNodeRepresentation = InstanceMethodCallFrameNodeRepresentation;
});
//#endregion
//#endregion
//# sourceMappingURL=NodeRepresentations.js.map