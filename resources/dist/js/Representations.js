///<reference path="definitions.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function (require, exports) {
    // Trim every textnode but ignore non-breakable spaces
    function trimHtml(element) {
        $(element).contents().filter(function () {
            return this.nodeType == Node.TEXT_NODE;
        }).each(function (i, el) {
            var newValue = el.nodeValue.replace(/^[^\S\u00A0]+|[^\S\u00A0]+$/g, '');
            if (newValue !== "")
                el.nodeValue = newValue;
            else
                el.parentNode.removeChild(el);
        });
    }
    exports.trimHtml = trimHtml;
    var NodeRepresentation = (function () {
        function NodeRepresentation(node, parentElement, representationStorage) {
            if (representationStorage === void 0) { representationStorage = null; }
            this.node = node;
            this.parentElement = parentElement;
            this.representationStorage = representationStorage;
        }
        NodeRepresentation.prototype.getNode = function () {
            return this.node;
        };
        NodeRepresentation.prototype.getParentElement = function () {
            return this.parentElement;
        };
        NodeRepresentation.prototype.getRepresentationStorage = function () {
            return this.representationStorage;
        };
        return NodeRepresentation;
    })();
    exports.NodeRepresentation = NodeRepresentation;
    // FIX for isComplexRepresentaion function
    var ComplexNodeRepresentation = (function (_super) {
        __extends(ComplexNodeRepresentation, _super);
        function ComplexNodeRepresentation() {
            _super.apply(this, arguments);
        }
        ComplexNodeRepresentation.prototype.open = function (id) {
            throw "Abstract function not implemented";
        };
        ComplexNodeRepresentation.prototype.close = function (id, immidiate) {
            throw "Abstract function not implemented";
        };
        ComplexNodeRepresentation.prototype.switch = function (id) {
            throw "Abstract function not implemented";
        };
        ComplexNodeRepresentation.prototype.reset = function (id) {
            throw "Abstract function not implemented";
        };
        ComplexNodeRepresentation.prototype.getState = function () {
            throw "Abstract function not implemented";
        };
        ComplexNodeRepresentation.prototype.setState = function (state) {
            throw "Abstract function not implemented";
        };
        return ComplexNodeRepresentation;
    })(NodeRepresentation);
    exports.ComplexNodeRepresentation = ComplexNodeRepresentation;
    function switchStateClick(event) {
        //var srcElement = $(event.target);
        var data = event.data;
        var _this = data.representation;
        _this.switch(data.id);
        if (_this.getRepresentationStorage() !== null)
            _this.getRepresentationStorage().saveCurrentState();
        // TODO scroll to element?
        //$("html, body").scrollTop(srcElement.offset().top);
        return false;
    }
    exports.switchStateClick = switchStateClick;
    function resetStateClick(event) {
        //var srcElement = $(event.target);
        var data = event.data;
        var _this = data.representation;
        _this.reset(data.id);
        _this.open(data.id);
        if (_this.getRepresentationStorage() !== null)
            _this.getRepresentationStorage().saveCurrentState();
        // TODO scroll to element?
        //$("html, body").scrollTop(srcElement.offset().top);
        return false;
    }
    exports.resetStateClick = resetStateClick;
    function scrollToOriginalEvent(event) {
        $(document).scrollTop(event.data.offset().top);
        if (event.data[0].tagName === "DT") {
            var rStart = 98, gStart = 147, bStart = 211;
            // Finish old animation and set initial css
            event.data.finish().css("background-color", "rgb(" + rStart + "," + gStart + "," + bStart + ")");
            event.data.animate({ backgroundColor: "" }, {
                duration: 800,
                progress: function (animation, progress, remainingMs) {
                    var r = Math.floor((255 - rStart) * progress) + rStart, g = Math.floor((255 - gStart) * progress) + gStart, b = Math.floor((255 - bStart) * progress) + bStart;
                    event.data.css("background-color", "rgb(" + r + "," + g + "," + b + ")");
                },
                always: function () {
                    event.data.css("background-color", ""); // Remove css after animation
                }
            });
        }
        else
            console.log("wrong element");
        return false;
    }
    exports.scrollToOriginalEvent = scrollToOriginalEvent;
    ;
    function animateShow(element, immidiate, finishCallback) {
        if (immidiate === void 0) { immidiate = false; }
        if (finishCallback === void 0) { finishCallback = null; }
        element.stop(true, false);
        element.show({
            duration: immidiate ? 0 : 300,
            always: finishCallback,
            complete: function () {
                element.removeAttr("style"); // Remove uncomplete animations
            }
        });
    }
    exports.animateShow = animateShow;
    function animateHide(element, immidiate, emptyAfterwards, finishCallback) {
        if (immidiate === void 0) { immidiate = false; }
        if (emptyAfterwards === void 0) { emptyAfterwards = false; }
        if (finishCallback === void 0) { finishCallback = null; }
        element.stop(true, false);
        element.hide({
            duration: immidiate ? 0 : 300,
            always: function () {
                if (emptyAfterwards)
                    element.empty();
                if (finishCallback !== null)
                    finishCallback();
            }
        });
    }
    exports.animateHide = animateHide;
});
//# sourceMappingURL=Representations.js.map