///<reference path="definitions.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "Nodes", "NodeRepresentations"], function (require, exports, Nodes, NodeRepresentations) {
    function convertToRepresentation(node, nodes, parent, parentElement, representationStorage, elementReferences) {
        if (elementReferences === void 0) { elementReferences = {}; }
        if (!(node.getId() in elementReferences))
            elementReferences[node.getId()] = parent;
        var registry = new NodeConverterRegistry(nodes);
        registry.registerConverter(new TraceValueNodeConverter(registry, representationStorage));
        registry.registerConverter(new ObjectNodeConverter(registry, representationStorage));
        registry.registerConverter(new PrimitiveNodeConverter());
        registry.registerConverter(new ArrayNodeConverter(registry, representationStorage));
        registry.registerConverter(new UnknownNodeConverter());
        registry.registerConverter(new ReferenceNodeConverter(registry));
        registry.registerConverter(new AbortDepthNodeConverter());
        return registry.convert(node, parentElement, elementReferences);
    }
    exports.convertToRepresentation = convertToRepresentation;
    //#region Abstract Converter
    var NodeConverter = (function () {
        function NodeConverter() {
        }
        NodeConverter.prototype.isAccepted = function (node) {
            throw "Abstract function not implemented";
        };
        NodeConverter.prototype.getId = function () {
            throw "Abstract function not implemented";
        };
        NodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            throw "Abstract function not implemented";
        };
        return NodeConverter;
    })();
    exports.NodeConverter = NodeConverter;
    //#endregion
    var NodeConverterRegistry = (function (_super) {
        __extends(NodeConverterRegistry, _super);
        function NodeConverterRegistry(nodes) {
            _super.call(this);
            this.nodes = nodes;
            this.nodeConverters = {};
        }
        NodeConverterRegistry.prototype.isAccepted = function (node) {
            for (var key in this.nodeConverters) {
                if (this.nodeConverters[key].isAccepted(node))
                    return true;
            }
            return false;
        };
        NodeConverterRegistry.prototype.getConverterForNode = function (node) {
            for (var key in this.nodeConverters) {
                if (this.nodeConverters[key].isAccepted(node))
                    return this.nodeConverters[key];
            }
            throw "Node type '" + node.getType() + "' not supported";
        };
        NodeConverterRegistry.prototype.registerConverter = function (converter) {
            if (typeof this.nodeConverters[converter.getId()] !== "undefined")
                throw "Converter type already registered";
            this.nodeConverters[converter.getId()] = converter;
        };
        NodeConverterRegistry.prototype.unregisterConverter = function (converter) {
            if (typeof this.nodeConverters[converter.getId()] === "undefined")
                throw "Converter is not yet registered";
            delete this.nodeConverters[converter.getId()];
        };
        NodeConverterRegistry.prototype.convert = function (node, parentElement, elementReferences, representationStorage) {
            if (elementReferences === void 0) { elementReferences = {}; }
            if (representationStorage === void 0) { representationStorage = null; }
            return this.getConverterForNode(node).convert(node, parentElement, elementReferences);
        };
        NodeConverterRegistry.prototype.getNode = function (id) {
            if (!(id in this.nodes))
                throw "Node with id '" + id + "' unknown";
            return this.nodes[id];
        };
        NodeConverterRegistry.prototype.getId = function () {
            return "NodeConverterReistry";
        };
        return NodeConverterRegistry;
    })(NodeConverter);
    exports.NodeConverterRegistry = NodeConverterRegistry;
    //#region Simple Converter
    var AbortDepthNodeConverter = (function (_super) {
        __extends(AbortDepthNodeConverter, _super);
        function AbortDepthNodeConverter() {
            _super.apply(this, arguments);
        }
        AbortDepthNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.AbortDepthNode;
        };
        AbortDepthNodeConverter.prototype.getId = function () {
            return "AbortDepthNodeConverter";
        };
        AbortDepthNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            return new NodeRepresentations.AbortDepthNodeRepresentation(node, parentElement);
        };
        return AbortDepthNodeConverter;
    })(NodeConverter);
    exports.AbortDepthNodeConverter = AbortDepthNodeConverter;
    var UnknownNodeConverter = (function (_super) {
        __extends(UnknownNodeConverter, _super);
        function UnknownNodeConverter() {
            _super.apply(this, arguments);
        }
        UnknownNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.UnknownNode;
        };
        UnknownNodeConverter.prototype.getId = function () {
            return "UnknownNodeConverter";
        };
        UnknownNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            return new NodeRepresentations.UnknownNodeRepresentation(node, parentElement);
        };
        return UnknownNodeConverter;
    })(NodeConverter);
    exports.UnknownNodeConverter = UnknownNodeConverter;
    var PrimitiveNodeConverter = (function (_super) {
        __extends(PrimitiveNodeConverter, _super);
        function PrimitiveNodeConverter() {
            _super.apply(this, arguments);
        }
        PrimitiveNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.PrimitiveNode;
        };
        PrimitiveNodeConverter.prototype.getId = function () {
            return "PrimitiveNodeConverter";
        };
        PrimitiveNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            return new NodeRepresentations.PrimitiveNodeRepresentation(node, parentElement);
        };
        return PrimitiveNodeConverter;
    })(NodeConverter);
    exports.PrimitiveNodeConverter = PrimitiveNodeConverter;
    //#endregion
    //#region Complex Converter
    var ReferenceNodeConverter = (function (_super) {
        __extends(ReferenceNodeConverter, _super);
        function ReferenceNodeConverter(nodeConverterRegistry) {
            _super.call(this);
            this.nodeConverterRegistry = nodeConverterRegistry;
        }
        ReferenceNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.ReferenceNode;
        };
        ReferenceNodeConverter.prototype.getId = function () {
            return "ReferenceNodeConverter";
        };
        ReferenceNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            var referencedNode = this.nodeConverterRegistry.getNode(node.getReferencedId());
            if (referencedNode !== null)
                return this.nodeConverterRegistry.convert(referencedNode, parentElement, elementReferences);
            else
                return null;
        };
        return ReferenceNodeConverter;
    })(NodeConverter);
    exports.ReferenceNodeConverter = ReferenceNodeConverter;
    var ArrayNodeConverter = (function (_super) {
        __extends(ArrayNodeConverter, _super);
        function ArrayNodeConverter(nodeConverterRegistry, representationStorage) {
            _super.call(this);
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.representationStorage = representationStorage;
        }
        ArrayNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.ArrayNode;
        };
        ArrayNodeConverter.prototype.getId = function () {
            return "ArrayNodeConverter";
        };
        ArrayNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            return new NodeRepresentations.ArrayNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        };
        return ArrayNodeConverter;
    })(NodeConverter);
    exports.ArrayNodeConverter = ArrayNodeConverter;
    var ObjectNodeConverter = (function (_super) {
        __extends(ObjectNodeConverter, _super);
        function ObjectNodeConverter(nodeConverterRegistry, representationStorage) {
            _super.call(this);
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.representationStorage = representationStorage;
        }
        ObjectNodeConverter.prototype.isAccepted = function (node) {
            return node instanceof Nodes.ObjectNode;
        };
        ObjectNodeConverter.prototype.getId = function () {
            return "ObjectNodeConverter";
        };
        ObjectNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            return new NodeRepresentations.ObjectNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
        };
        return ObjectNodeConverter;
    })(NodeConverter);
    exports.ObjectNodeConverter = ObjectNodeConverter;
    var TraceValueNodeConverter = (function (_super) {
        __extends(TraceValueNodeConverter, _super);
        function TraceValueNodeConverter(nodeConverterRegistry, representationStorage) {
            _super.call(this);
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.representationStorage = representationStorage;
        }
        TraceValueNodeConverter.prototype.isAccepted = function (node) {
            return (node instanceof Nodes.StackTraceNode) || (node instanceof Nodes.CallFrameNode);
        };
        TraceValueNodeConverter.prototype.getId = function () {
            return "TraceValueNodeConverter";
        };
        TraceValueNodeConverter.prototype.convert = function (node, parentElement, elementReferences) {
            if (node instanceof Nodes.StackTraceNode)
                return new NodeRepresentations.StackTraceNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
            else if (node instanceof Nodes.ClosureCallFrameNode)
                return new NodeRepresentations.ClosureCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
            else if (node instanceof Nodes.FunctionCallFrameNode)
                return new NodeRepresentations.FunctionCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
            else if (node instanceof Nodes.StaticMethodCallFrameNode)
                return new NodeRepresentations.StaticMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
            else if (node instanceof Nodes.InstanceMethodCallFrameNode)
                return new NodeRepresentations.InstanceMethodCallFrameNodeRepresentation(node, parentElement, elementReferences, this.nodeConverterRegistry, this.representationStorage);
            else
                throw "node not supported";
        };
        return TraceValueNodeConverter;
    })(NodeConverter);
    exports.TraceValueNodeConverter = TraceValueNodeConverter;
});
//#endregion
//# sourceMappingURL=NodeConverters.js.map