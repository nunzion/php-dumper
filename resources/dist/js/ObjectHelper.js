define(["require", "exports"], function (require, exports) {
    function map(obj, mapFunc) {
        var result = {};
        for (var key in obj) {
            var value = obj[key];
            result[key] = mapFunc(value, key);
        }
        return result;
    }
    exports.map = map;
    function clone(obj) {
        return Object.create(obj);
    }
    exports.clone = clone;
    function getPropertyValues(obj) {
        return Object.keys(obj).map(function (key) {
            return obj[key];
        });
    }
    exports.getPropertyValues = getPropertyValues;
});
//# sourceMappingURL=ObjectHelper.js.map