///<reference path="definitions.d.ts"/>
define(["require", "exports", "jquery", "Template", "Nodes", "NodeFactories", "Representations", "TabRepresentations", "NodeConverters"], function (require, exports, $, Template, Nodes, NodeFactories, Representations, TabRepresentations, NodeConverters) {
    var SessionStorageRepresentationState = (function () {
        function SessionStorageRepresentationState(rootRepresentation) {
            this.rootRepresentation = rootRepresentation;
        }
        SessionStorageRepresentationState.prototype.saveCurrentState = function () {
            var state = this.rootRepresentation.getState();
            sessionStorage.setItem("nunzion-php-dump-" + this.rootRepresentation.getId(), JSON.stringify(state));
        };
        SessionStorageRepresentationState.prototype.restoreSavedState = function () {
            var session = sessionStorage.getItem("nunzion-php-dump-" + this.rootRepresentation.getId());
            var state;
            if ((typeof session !== "undefined") && ((state = JSON.parse(session)) !== null)) {
                this.rootRepresentation.setState(state);
            }
            else {
                this.rootRepresentation.setState({ type: null });
            }
        };
        return SessionStorageRepresentationState;
    })();
    var JavascriptHtmlDumper = (function () {
        function JavascriptHtmlDumper(node, nodes, trace, callingLine, parentElement, id, representationStorageFactory) {
            this.node = node;
            this.nodes = nodes;
            this.trace = trace;
            this.callingLine = callingLine;
            this.parentElement = parentElement;
            this.id = id;
            this.nodeRepresentation = null;
            this.nodeOpen = false;
            this.nodeNav = null;
            this.traceRepresentation = null;
            this.traceOpen = false;
            this.traceNav = null;
            // get and render template
            var tmpl = Template.get("JavascriptHtmlDumper");
            var data = { containsTrace: trace !== null, expandable: node.isExpandable() };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            var elements = $(html);
            parent.append(elements);
            Representations.trimHtml(parentElement);
            // add node and trace to nodes
            if ((nodes[node.getId()] === undefined) || (nodes[node.getId()] === null))
                this.nodes[node.getId()] = node;
            if ((trace !== null) && ((nodes[trace.getId()] === undefined) || (nodes[trace.getId()] === null)))
                this.nodes[trace.getId()] = trace;
            this.nodeHeaderParent = elements.filter("div.javascript-html-header").eq(0);
            Representations.trimHtml(this.nodeHeaderParent[0]);
            this.representationStorage = representationStorageFactory(this);
            this.nodeParent = elements.filter("div.javascript-html-dump").eq(0);
            this.nodeParent.hide();
            // print header
            if ((!JavascriptHtmlDumper.showCallingLineOnlyForComplex || (node instanceof Nodes.ComplexNode)) && (callingLine !== null))
                this.nodeHeaderParent.children("span.outline").html(Template.htmlEncode(callingLine, false));
            else if (node instanceof Nodes.ComplexNode)
                this.nodeHeaderParent.children("span.outline").html((trace === null) ?
                    "<span class=\"function\">dump</span>(" + node.getOutline() + ");"
                    : trace.getMostRecentCallFrame().getOutline());
            else
                this.nodeHeaderParent.children("span.outline").html(node.getOutline());
            // add possible nav
            if (data.expandable) {
                if (!(node instanceof Nodes.ComplexNode))
                    this.nodeParent.addClass("simpleNode");
                this.nodeNav = this.nodeHeaderParent.children("nav").eq(0);
                this.nodeNav.click({ representation: this, id: "node" }, Representations.switchStateClick)
                    .dblclick({ representation: this, id: "node" }, Representations.resetStateClick);
            }
            else
                this.nodeHeaderParent.addClass("simpleNode");
            // process trace part
            this.traceHeaderParent = elements.filter("div.javascript-html-called-from-header").eq(0);
            this.traceParent = elements.filter("div.javascript-html-called-from").eq(0);
            this.traceParent.hide();
            if (trace !== null) {
                Representations.trimHtml(this.traceHeaderParent[0]);
                this.traceNav = this.traceHeaderParent.children("nav").eq(0);
                this.traceNav.click({ representation: this, id: "trace" }, Representations.switchStateClick)
                    .dblclick({ representation: this, id: "trace" }, Representations.resetStateClick);
                // print header
                var source = trace.getMostRecentCallFrame().getSource();
                var sourceLine = trace.getMostRecentCallFrame().getSourceLine();
                var header = "<span class=\"calledFrom\"> called from ";
                if (source === null)
                    header += "*unknown*";
                else if (source instanceof Nodes.FileSource) {
                    var path = source.getPath();
                    var lastSep = Math.max(path.lastIndexOf("\\"), path.lastIndexOf("/"));
                    header += "<span class=\"mouseoverGreyed\">" + path.slice(0, lastSep + 1) + "</span>" + path.substr(lastSep + 1);
                }
                else
                    header += source.getSourceType().charAt(0).toLocaleLowerCase() + source.getSourceType().slice(1, -6);
                if (sourceLine !== null)
                    header += ":" + sourceLine;
                header += "</span>";
                this.traceHeaderParent.children("span.outline").html(header);
            }
            // open/close last state
            this.representationStorage.restoreSavedState();
        }
        Object.defineProperty(JavascriptHtmlDumper, "showCallingLineOnlyForComplex", {
            get: function () { return true; },
            enumerable: true,
            configurable: true
        });
        JavascriptHtmlDumper.prototype.getParentElement = function () {
            return this.parentElement;
        };
        JavascriptHtmlDumper.prototype.getRepresentationStorage = function () {
            return this.representationStorage;
        };
        JavascriptHtmlDumper.prototype.getNodes = function () {
            return this.nodes;
        };
        JavascriptHtmlDumper.prototype.getNodeById = function (id) {
            return this.nodes[id];
        };
        JavascriptHtmlDumper.prototype.getId = function () {
            return this.id;
        };
        JavascriptHtmlDumper.prototype.open = function (id) {
            var _this = this;
            if ((id === "node") && this.node.isExpandable()) {
                if (this.nodeRepresentation === null) {
                    Representations.animateHide(this.nodeParent, true);
                    var elementReferences = {};
                    elementReferences[this.node.getId()] = this.nodeHeaderParent.add(this.nodeParent);
                    this.nodeRepresentation = NodeConverters.convertToRepresentation(this.node, this.nodes, this.nodeHeaderParent.add(this.nodeParent), this.nodeParent[0], this.representationStorage, elementReferences);
                }
                Representations.animateShow(this.nodeParent);
                this.nodeOpen = true;
                this.nodeNav.addClass("open");
                return this.nodeRepresentation;
            }
            else if ((id === "trace") && (this.trace !== null)) {
                if (this.traceRepresentation === null) {
                    Representations.animateHide(this.traceParent, true, false, function () { _this.traceHeaderParent.removeClass("show"); });
                    var elementReferences = {};
                    elementReferences[this.trace.getId()] = this.traceHeaderParent.add(this.traceParent);
                    this.traceRepresentation = NodeConverters.convertToRepresentation(this.trace, this.nodes, this.nodeHeaderParent.add(this.nodeParent), this.traceParent[0], this.representationStorage, elementReferences);
                }
                Representations.animateShow(this.traceParent);
                this.traceHeaderParent.addClass("show");
                this.traceOpen = true;
                this.traceNav.addClass("open");
                return this.traceRepresentation;
            }
            else
                return null;
        };
        JavascriptHtmlDumper.prototype.close = function (id, immidiate) {
            var _this = this;
            if (immidiate === void 0) { immidiate = false; }
            if ((id === "node") && this.node.isExpandable()) {
                Representations.animateHide(this.nodeParent);
                this.nodeOpen = false;
                this.nodeNav.removeClass("open");
            }
            else if ((id === "trace") && (this.trace !== null)) {
                Representations.animateHide(this.traceParent, false, false, function () { _this.traceHeaderParent.removeClass("show"); });
                this.traceOpen = false;
                this.traceNav.removeClass("open");
            }
            else
                return false;
            return true;
        };
        JavascriptHtmlDumper.prototype.switch = function (id) {
            var open;
            if (id === "node")
                open = this.nodeOpen;
            else if (id === "trace")
                open = this.traceOpen;
            else
                return;
            if (open)
                this.close(id);
            else
                this.open(id);
        };
        JavascriptHtmlDumper.prototype.reset = function (id) {
            var _this = this;
            if ((id === "node") && this.node.isExpandable()) {
                Representations.animateHide(this.nodeParent, false, true);
                this.nodeOpen = false;
                this.nodeRepresentation = null;
                this.nodeNav.removeClass("open");
            }
            else if ((id === "trace") && (this.trace !== null)) {
                Representations.animateHide(this.traceParent, false, true, function () { _this.traceHeaderParent.removeClass("show"); });
                this.traceOpen = false;
                this.traceRepresentation = null;
                this.traceNav.removeClass("open");
            }
        };
        JavascriptHtmlDumper.prototype.getState = function () {
            var node = null, nodeComplex, trace = null;
            if ((this.nodeRepresentation !== null) && (nodeComplex = TabRepresentations.isComplexRepresentation(this.nodeRepresentation)))
                node = this.nodeRepresentation.getState();
            else if (this.nodeRepresentation !== null)
                node = this.node.getCriteriaString();
            if (this.traceRepresentation !== null)
                trace = this.traceRepresentation.getState();
            return { type: "JavascriptHtmlDumper", node: node, nodeComplex: nodeComplex, nodeOpen: this.nodeOpen, trace: trace, traceOpen: this.traceOpen };
        };
        JavascriptHtmlDumper.prototype.setState = function (state) {
            if (state.type === "JavascriptHtmlDumper") {
                if (state.node !== null) {
                    this.open("node");
                    if (!state.nodeOpen)
                        this.close("node", true);
                    if (TabRepresentations.isComplexRepresentation(this.nodeRepresentation) && state.nodeComplex)
                        this.nodeRepresentation.setState(state.node);
                    else if (TabRepresentations.isComplexRepresentation(this.nodeRepresentation))
                        this.nodeRepresentation.setState({ type: null });
                    else if (this.node.getCriteriaString() !== state.node)
                        this.close("node", true);
                }
                else
                    this.reset("node");
                if ((this.trace !== null) && (state.trace !== null)) {
                    this.open("trace");
                    if (!state.traceOpen)
                        this.close("trace", true);
                    this.traceRepresentation.setState(state.trace);
                }
                else
                    this.reset("trace");
            }
            else {
                this.close("node", true);
                this.reset("node");
                this.close("trace", true);
                this.reset("trace");
            }
        };
        return JavascriptHtmlDumper;
    })();
    exports.JavascriptHtmlDumper = JavascriptHtmlDumper;
    $(document).ready(function () {
        // Create wait modal
        var modal = $("<div>");
        modal.addClass("nunzion-php-dump-wait-modal");
        modal.css('background-image', 'url(' + require.toUrl('') + "../../images/wait.gif" + ')');
        modal.click(function () {
            modal.remove(); // Allow user to ignore message
        });
        modal.appendTo("body");
        var ready = 0, callback = function () {
            ready++;
            if (ready < 2)
                return;
            try {
                $(".nunzion-php-dump").each(function (idx, element) {
                    var data = NodeFactories.getFromJson($(element).children(".data").text());
                    $(element).empty();
                    var dumper = new JavascriptHtmlDumper(data.root, data.nodes, data.trace, data.callingLine, element, data.id, function (rootRepresentation) { return new SessionStorageRepresentationState(rootRepresentation); });
                });
            }
            finally {
                modal.remove();
            }
        };
        Template.loadFromTemplateFile(require.toUrl('') + "../templates/Nodes.tmpl.html", callback);
        Template.loadFromTemplateFile(require.toUrl('') + "../templates/Tabs.tmpl.html", callback);
    });
});
//# sourceMappingURL=dumper.js.map