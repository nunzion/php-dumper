///<reference path="definitions.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "ObjectHelper", "Template", "Nodes", "Representations"], function (require, exports, ObjectHelper, Template, Nodes, Representations) {
    function getTabHeader(node) {
        var result = [];
        if (node instanceof Nodes.ArrayNode) {
            result = ["Elements (" + node.getElements().length + ")"];
        }
        else if (node instanceof Nodes.ObjectNode) {
            result = ["Properties (" + node.getElements().length + ")"];
        }
        else if (node instanceof Nodes.CallFrameNode) {
            result = ["Arguments (" + node.getElements().length + ")"];
            if (node.getSource() !== null)
                result.push(node.getSource().getSourceType().slice(0, -6));
        }
        return result;
    }
    exports.getTabHeader = getTabHeader;
    function createTab(tabbedNodeRepresentation, // TODO pass node
        tabContent, id) {
        var result = null;
        $(tabContent).empty();
        $(tabContent).hide();
        if (tabbedNodeRepresentation.getNode() instanceof Nodes.ArrayNode) {
            if (id === 0 || id === "elements") {
                result = new ArrayElementsTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent, tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
            }
        }
        else if (tabbedNodeRepresentation.getNode() instanceof Nodes.ObjectNode) {
            if (id === 0 || id === "properties") {
                result = new ObjectPropertiesTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent, tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
            }
        }
        else if (tabbedNodeRepresentation.getNode() instanceof Nodes.CallFrameNode) {
            if (id === 0 || id === "arguments") {
                result = new ArrayElementsTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent, tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
            }
            else if ((tabbedNodeRepresentation.getNode().getSource() !== null) && (id === 1 || id === "source")) {
                result = new SourceTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent, tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
            }
        }
        return result;
    }
    exports.createTab = createTab;
    // FIX for typescript/javascript not supporting interfaces in instanceof
    function isComplexRepresentation(object) {
        return object instanceof TabbedNodeRepresentation
            || object instanceof ComplexTabRepresentation
            || object instanceof Representations.ComplexNodeRepresentation;
    }
    exports.isComplexRepresentation = isComplexRepresentation;
    //#region Abstract Representations
    var TabbedNodeRepresentation = (function (_super) {
        __extends(TabbedNodeRepresentation, _super);
        function TabbedNodeRepresentation(node, parentElement, elementReferences, nodeConverterRegistry, representationStorage) {
            var _this = this;
            if (representationStorage === void 0) { representationStorage = null; }
            _super.call(this, node, parentElement, representationStorage);
            this.elementReferences = elementReferences;
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.tabs = [];
            this.activeTab = null;
            this.tabHeader = [];
            this.tabContents = [];
            this.type = "TabbedNode";
            // get and render template
            var tmpl = Template.get("TabList");
            var tabs = getTabHeader(node);
            var data = { tabs: tabs };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.hide();
            parent.html(html);
            Representations.trimHtml(parentElement);
            // add content to tabs
            parent.children("ul.header").children("li").each(function (i, el) { return _this.tabHeader[i] = $(el); });
            parent.children("ul.content").children("li").each(function (i, el) { return _this.tabContents[i] = el; });
            for (var i = 0; i < tabs.length; i++) {
                this.tabs[i] = createTab(this, this.tabContents[i], i);
                // set onclick & ondblclick
                this.tabHeader[i].click({ representation: this, id: i }, Representations.switchStateClick);
                this.tabHeader[i].dblclick({ representation: this, id: i }, Representations.resetStateClick);
            }
            Representations.animateShow(parent, false, function () {
                Representations.animateShow($(_this.tabContents[0]));
            });
            this.tabHeader[0].addClass("active");
            this.activeTab = 0;
        }
        TabbedNodeRepresentation.prototype.getTabs = function () {
            return this.tabs;
        };
        TabbedNodeRepresentation.prototype.getElementReferences = function () {
            return this.elementReferences;
        };
        TabbedNodeRepresentation.prototype.getNodeConverterRegistry = function () {
            return this.nodeConverterRegistry;
        };
        TabbedNodeRepresentation.prototype.open = function (id) {
            if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined")) {
                if (this.activeTab !== null)
                    this.close(this.activeTab, true);
                Representations.animateShow($(this.tabContents[id]));
                this.tabHeader[id].addClass("active");
                this.activeTab = id;
                return this.tabs[id];
            }
            else
                return null;
        };
        TabbedNodeRepresentation.prototype.close = function (id, immidiate) {
            var _this = this;
            if (immidiate === void 0) { immidiate = false; }
            if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined") && (this.activeTab === id)) {
                Representations.animateHide($(this.tabContents[id]), immidiate, false, function () { _this.tabHeader[id].removeClass("active"); });
                this.activeTab = null;
                return true;
            }
            else
                return false;
        };
        TabbedNodeRepresentation.prototype.switch = function (id) {
            if (typeof id !== "number")
                return;
            if (this.activeTab === id)
                this.close(id);
            else
                this.open(id);
        };
        TabbedNodeRepresentation.prototype.reset = function (id) {
            if (typeof id === "number") {
                if (this.activeTab !== null)
                    this.close(this.activeTab, true);
                this.tabs[id] = createTab(this, this.tabContents[id], id);
                this.open(id);
            }
        };
        TabbedNodeRepresentation.prototype.getState = function () {
            var result = { type: this.type, tabs: [], active: this.activeTab };
            this.tabs.forEach(function (tab, id) {
                if (isComplexRepresentation(tab)) {
                    result.tabs[id] = tab.getState();
                }
            });
            return result;
        };
        TabbedNodeRepresentation.prototype.setState = function (state) {
            var _this = this;
            if (state.type !== this.type) {
                this.tabs.forEach(function (tab, i) { return _this.reset(i); });
                for (var first in this.tabs)
                    break;
                first = +first; // convert to number if possible
                this.open(first);
            }
            else {
                // change state
                state.tabs.forEach(function (tab, id) {
                    if ((typeof _this.tabs[id] !== "undefined") && isComplexRepresentation(_this.tabs[id])) {
                        _this.tabs[id].setState(tab);
                    }
                });
                if (state.active === null) {
                    this.close(this.activeTab, true);
                }
                else if (!this.open(state.active)) {
                    for (var first in this.tabs)
                        break;
                    first = +first; // convert to number if possible
                    this.open(first);
                }
            }
        };
        return TabbedNodeRepresentation;
    })(Representations.NodeRepresentation);
    exports.TabbedNodeRepresentation = TabbedNodeRepresentation;
    var TabRepresentation = (function () {
        function TabRepresentation(node, representationStorage, parentElement) {
            this.node = node;
            this.representationStorage = representationStorage;
            this.parentElement = parentElement;
        }
        TabRepresentation.prototype.getNode = function () {
            return this.node;
        };
        TabRepresentation.prototype.getParentElement = function () {
            return this.parentElement;
        };
        TabRepresentation.prototype.getRepresentationStorage = function () {
            return this.representationStorage;
        };
        return TabRepresentation;
    })();
    exports.TabRepresentation = TabRepresentation;
    var ComplexTabRepresentation = (function (_super) {
        __extends(ComplexTabRepresentation, _super);
        function ComplexTabRepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement);
            this.elementReferences = elementReferences;
            this.nodeConverterRegistry = nodeConverterRegistry;
            this.elements = [];
            this.type = "ComplexTab";
        }
        ComplexTabRepresentation.prototype.getElementReferences = function () {
            return this.elementReferences;
        };
        ComplexTabRepresentation.prototype.getNodeConverterRegistry = function () {
            return this.nodeConverterRegistry;
        };
        ComplexTabRepresentation.prototype.getValue = function (id) {
            throw "Abstract function not implemented";
        };
        ComplexTabRepresentation.prototype.open = function (id) {
            if ((typeof id !== "number") || (this.elements[id] === undefined) || (this.elements[id].nav === null)) {
                return null;
            }
            else {
                this.elements[id].nav.addClass("open");
                if (this.elements[id].definitionRepresentation === null) {
                    Representations.animateHide(this.elements[id].definition, true, true);
                    var value = this.getValue(id);
                    var newReferences = ObjectHelper.clone(this.elementReferences);
                    newReferences[value.getId()] = this.elements[id].title.add(this.elements[id].definition);
                    this.elements[id].definitionRepresentation = this.nodeConverterRegistry.convert(value, this.elements[id].definition[0], newReferences, this.representationStorage);
                    this.elements[id].open = false;
                }
                if (!this.elements[id].open) {
                    Representations.animateShow(this.elements[id].definition);
                    this.elements[id].open = true;
                }
                return this.elements[id].definitionRepresentation;
            }
        };
        ComplexTabRepresentation.prototype.close = function (id, immidiate) {
            if (immidiate === void 0) { immidiate = false; }
            if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
                return false;
            }
            else {
                this.elements[id].nav.removeClass("open");
                Representations.animateHide(this.elements[id].definition, immidiate);
                this.elements[id].open = false;
                return true;
            }
        };
        ComplexTabRepresentation.prototype.reset = function (id) {
            if ((typeof id === "number") && (typeof this.elements[id] !== "undefined") && (this.elements[id].nav !== null)) {
                this.elements[id].nav.removeClass("open");
                Representations.animateHide(this.elements[id].definition, false, true);
                this.elements[id].definitionRepresentation = null;
                this.elements[id].open = false;
            }
        };
        ComplexTabRepresentation.prototype.switch = function (id) {
            if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
                return;
            }
            else {
                if (!this.elements[id].open)
                    this.open(id);
                else
                    this.close(id);
            }
        };
        ComplexTabRepresentation.prototype.getState = function () {
            var elements = this.elements.map(function (element, id) {
                if ((element.definitionRepresentation === null) || (element.nav === null))
                    return null;
                var node = element.definingNode;
                var state = null;
                if ((element.definitionRepresentation !== null) && isComplexRepresentation(element.definitionRepresentation))
                    state = element.definitionRepresentation.getState();
                return { id: id, open: element.open, nodeType: node.getNodeType(), criteria: node.getCriteriaString(), state: state };
            });
            return { type: this.type, elements: elements };
        };
        ComplexTabRepresentation.prototype.setState = function (state) {
            var _this = this;
            if ((state.type !== this.type) || (this.elements.length !== state.elements.length)) {
                this.elements.forEach(function (element, id) { return _this.reset(id); });
            }
            else {
                // change state
                this.elements.forEach(function (element, id) {
                    var node = element.definingNode;
                    if ((typeof state.elements[id] !== "undefined") && (state.elements[id] !== null) &&
                        (state.elements[id].nodeType === node.getNodeType()) && (state.elements[id].criteria === node.getCriteriaString())) {
                        _this.open(id);
                        if (!state.elements[id].open)
                            _this.close(id, true);
                        if ((element.definitionRepresentation !== null) && isComplexRepresentation(element.definitionRepresentation)
                            && (state.elements[id].state !== null))
                            element.definitionRepresentation.setState(state.elements[id].state);
                    }
                    else {
                        _this.reset(id);
                    }
                });
            }
        };
        return ComplexTabRepresentation;
    })(TabRepresentation);
    exports.ComplexTabRepresentation = ComplexTabRepresentation;
    //#endregion
    //#region Array TabRepresentations
    var ArrayElementsTabRepresentation = (function (_super) {
        __extends(ArrayElementsTabRepresentation, _super);
        function ArrayElementsTabRepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
            this.type = "ArrayElements";
            // TODO categories to filter
            // get and render template
            var tmpl = Template.get("ArrayElements");
            var elementReferences = elementReferences;
            var data = {
                elements: node.getElements(),
                isComplex: function (element) { return element instanceof Nodes.ComplexNode; },
                elementReferences: elementReferences
            };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.html(html);
            Representations.trimHtml(parentElement);
            // add converted elements
            var titles = parent.children("dl").children("dt");
            titles.each(function (i, el) { return Representations.trimHtml(el); });
            var definitions = parent.children("dl").children("dd");
            definitions.empty();
            for (var key in data.elements) {
                key = +key; // convert to number
                var element = data.elements[key];
                var title = titles.eq(key).children("span.term");
                var definition;
                var nav = null;
                var titleRepresentation;
                var definitionRepresentation = null;
                var newReferences = ObjectHelper.clone(elementReferences);
                newReferences[element.getValue().getId()] = titles.eq(key).add(definitions.eq(key));
                // add key content
                titleRepresentation = nodeConverterRegistry.convert(element.getKey(), title[0], newReferences, this.getRepresentationStorage());
                if (element.getValue() instanceof Nodes.ComplexNode) {
                    // add onclick & ondblclick to nav
                    nav = titles.eq(key).children("nav").eq(0);
                    nav.click({ representation: this, id: key }, Representations.switchStateClick);
                    nav.dblclick({ representation: this, id: key }, Representations.resetStateClick);
                    // add onclick to span.id if duplicate
                    if (element.getValue().getId() in elementReferences) {
                        var id = titles.eq(key).children("span.id").eq(0);
                        var er = elementReferences[element.getValue().getId()];
                        id.click(er, Representations.scrollToOriginalEvent);
                        id.hover(function (event) { er.addClass("markedDuplicate"); }, function (event) { er.removeClass("markedDuplicate"); });
                    }
                    definition = definitions.eq(key);
                }
                else {
                    definition = titles.eq(key).children("span.definition");
                    // add value content
                    definitionRepresentation = nodeConverterRegistry.convert(element.getValue(), definition[0], newReferences, this.getRepresentationStorage());
                }
                // copy classes to title
                definitions.eq(key).attr('class').split(/\s+/).forEach(function (className) { return titles.eq(key).addClass(className); });
                // save element references
                this.elements[key] = {
                    title: title, definition: definition, nav: nav, open: !(element.getValue() instanceof Nodes.ComplexNode),
                    titleRepresentation: titleRepresentation, definitionRepresentation: definitionRepresentation,
                    definingNode: element.getKey()
                };
            }
        }
        ArrayElementsTabRepresentation.prototype.getValue = function (id) {
            return this.node.getElements()[id].getValue();
        };
        return ArrayElementsTabRepresentation;
    })(ComplexTabRepresentation);
    exports.ArrayElementsTabRepresentation = ArrayElementsTabRepresentation;
    //#endregion
    //#region Object TabRepresentations
    var ObjectPropertiesTabRepresentation = (function (_super) {
        __extends(ObjectPropertiesTabRepresentation, _super);
        function ObjectPropertiesTabRepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
            this.type = "ObjectProperties";
            // TODO categories to filter
            // get and render template
            var tmpl = Template.get("ObjectProperties");
            var elementReferences = elementReferences;
            // sort properties by visibility
            this.sortedProperties = node.getElements().sort(function (a, b) {
                return ObjectPropertiesTabRepresentation.getPropertyWeight(a) - ObjectPropertiesTabRepresentation.getPropertyWeight(b);
            });
            var data = {
                properties: this.sortedProperties,
                isComplex: function (element) { return element instanceof Nodes.ComplexNode; },
                elementReferences: elementReferences,
                declaringClass: node.getType()
            };
            var html = tmpl.render(data);
            // add to parent
            var parent = $(parentElement);
            parent.html(html);
            Representations.trimHtml(parentElement);
            // add converted elements
            var titles = parent.children("dl").children("dt");
            titles.each(function (i, el) { return Representations.trimHtml(el); });
            var definitions = parent.children("dl").children("dd");
            definitions.each(function (i, el) { return Representations.trimHtml(el); });
            for (var key in data.properties) {
                key = +key; // convert to number
                var property = data.properties[key];
                var definition;
                var nav = null;
                var definitionRepresentation = null;
                var newReferences = ObjectHelper.clone(elementReferences);
                newReferences[property.getValue().getId()] = titles.eq(key).add(definitions.eq(key));
                if (property.getValue() instanceof Nodes.ComplexNode) {
                    // add onclick & ondblclick to nav
                    nav = titles.eq(key).children("nav").eq(0);
                    nav.click({ representation: this, id: key }, Representations.switchStateClick);
                    nav.dblclick({ representation: this, id: key }, Representations.resetStateClick);
                    // add onclick to span.id if duplicate
                    if (property.getValue().getId() in elementReferences) {
                        var id = titles.eq(key).children("span.id").eq(0);
                        var er = elementReferences[property.getValue().getId()];
                        id.click(er, Representations.scrollToOriginalEvent);
                        id.hover(function (event) { er.addClass("markedDuplicate"); }, function (event) { er.removeClass("markedDuplicate"); });
                    }
                    definition = definitions.eq(key);
                }
                else {
                    definition = titles.eq(key).children("span.definition");
                    titles.eq(key).children("span.term").each(function (i, el) { return Representations.trimHtml(el); });
                    // add value content
                    definitionRepresentation = nodeConverterRegistry.convert(property.getValue(), definition[0], newReferences, this.getRepresentationStorage());
                }
                // copy classes to title
                definitions.eq(key).attr('class').split(/\s+/).forEach(function (className) { return titles.eq(key).addClass(className); });
                // save element references
                this.elements[key] = {
                    title: titles.eq(key), definition: definition, nav: nav, open: !(property.getValue() instanceof Nodes.ComplexNode),
                    titleRepresentation: null, definitionRepresentation: definitionRepresentation,
                    definingNode: property.getValue()
                };
            }
        }
        ObjectPropertiesTabRepresentation.getPropertyWeight = function (property) {
            var result = property.getIsStatic() ? 0 : 100;
            if (property.getVisibility() === "private")
                result = result + 10;
            if (property.getVisibility() === "protected")
                result = result + 20;
            if (property.getVisibility() === "public")
                result = result + 30;
            return result;
        };
        ObjectPropertiesTabRepresentation.prototype.getValue = function (id) {
            return this.sortedProperties[id].getValue();
        };
        return ObjectPropertiesTabRepresentation;
    })(ComplexTabRepresentation);
    exports.ObjectPropertiesTabRepresentation = ObjectPropertiesTabRepresentation;
    //#endregion
    //#region CallFrames TabRepresentations
    var CallFrameArgumentsTabRepresentation = (function (_super) {
        __extends(CallFrameArgumentsTabRepresentation, _super);
        function CallFrameArgumentsTabRepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
            this.arrayElements = new ArrayElementsTabRepresentation(node.getArgs(), representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        }
        CallFrameArgumentsTabRepresentation.prototype.getArrayElementTabRepresentation = function () {
            return this.arrayElements;
        };
        CallFrameArgumentsTabRepresentation.prototype.getValue = function (id) {
            throw "Should be unused";
        };
        CallFrameArgumentsTabRepresentation.prototype.open = function (id) {
            throw "Should be unused";
        };
        CallFrameArgumentsTabRepresentation.prototype.close = function (id, immidiate) {
            throw "Should be unused";
        };
        CallFrameArgumentsTabRepresentation.prototype.reset = function (id) {
            throw "Should be unused";
        };
        CallFrameArgumentsTabRepresentation.prototype.switch = function (id) {
            throw "Should be unused";
        };
        CallFrameArgumentsTabRepresentation.prototype.getState = function () {
            return { type: "CallFrameArguments", arrayElements: this.arrayElements.getState(), elements: [] };
        };
        CallFrameArgumentsTabRepresentation.prototype.setState = function (state) {
            if ((state.type === "CallFrameArguments")) {
                this.arrayElements.setState(state.arrayElements);
            }
            else {
                this.arrayElements.setState({ type: null, elements: [] });
            }
        };
        return CallFrameArgumentsTabRepresentation;
    })(ComplexTabRepresentation);
    exports.CallFrameArgumentsTabRepresentation = CallFrameArgumentsTabRepresentation;
    var SourceTabRepresentation = (function (_super) {
        __extends(SourceTabRepresentation, _super);
        function SourceTabRepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement);
            if (node.getSource() === null)
                throw "Source cannot be null";
            var parent = $(parentElement);
            parent.empty();
            parent.addClass("source");
            if (node.getSource() instanceof Nodes.FileSource)
                parent.append($("<span>").addClass("path").text(node.getSource().getPath()));
            var content = parent.append("<div>").addClass("content");
            nodeConverterRegistry.convert(node.getSource().getContentNode(), content[0], elementReferences, representationStorage);
        }
        return SourceTabRepresentation;
    })(TabRepresentation);
    exports.SourceTabRepresentation = SourceTabRepresentation;
    var ClosureHandleTabrepresentation = (function (_super) {
        __extends(ClosureHandleTabrepresentation, _super);
        function ClosureHandleTabrepresentation(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry) {
            _super.call(this, node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        }
        return ClosureHandleTabrepresentation;
    })(ComplexTabRepresentation);
    exports.ClosureHandleTabrepresentation = ClosureHandleTabrepresentation;
});
//#endregion
//# sourceMappingURL=TabRepresentations.js.map