///<reference path="definitions.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "Nodes", "ObjectHelper"], function (require, exports, Nodes, ObjectHelper) {
    function getFromJson(json) {
        var data = JSON.parse(json);
        if (data["@type"] !== "javascriptHtmlDumper")
            throw "Unsupported type " + data["@type"];
        var nodes = {};
        var registry = new NodeFactoryRegistry();
        registry.registerFactory(new ObjectNodeFactory(registry, nodes));
        registry.registerFactory(new PrimitiveNodeFactory());
        registry.registerFactory(new ArrayNodeFactory(registry, nodes));
        registry.registerFactory(new UnknownNodeFactory());
        registry.registerFactory(new ReferenceNodeFactory(registry, nodes));
        registry.registerFactory(new AbortDepthNodeFactory());
        registry.registerFactory(new StackTraceFactory(registry, nodes));
        registry.registerFactory(new CallFrameFactory(registry, nodes));
        var root = registry.createFromJson(data.root);
        var trace = null;
        var newNodes = {};
        newNodes[root.getId()] = root;
        if ((data.trace !== null) && (data.trace !== undefined)) {
            trace = registry.createFromJson(data.trace);
            newNodes[trace.getId()] = trace;
        }
        $.extend(nodes, newNodes, ObjectHelper.map(data.nodes, function (node) { return registry.createFromJson(node); }));
        while (root instanceof Nodes.ReferenceNode)
            root = root.getReferencedNode();
        while ((trace !== null) && (trace instanceof Nodes.ReferenceNode))
            trace = trace.getReferencedNode();
        return { id: data.id, root: root, nodes: nodes, trace: trace, callingLine: data.callingLine };
    }
    exports.getFromJson = getFromJson;
    //#region Abstract Factories
    var NodeFactory = (function () {
        function NodeFactory() {
        }
        NodeFactory.prototype.getAcceptedType = function () {
            return null;
        };
        NodeFactory.prototype.createFromJson = function (json) {
            throw "Abstract function not implemented";
        };
        return NodeFactory;
    })();
    exports.NodeFactory = NodeFactory;
    var ComplexNodeFactory = (function (_super) {
        __extends(ComplexNodeFactory, _super);
        function ComplexNodeFactory(nodeFactory, nodeReferences) {
            _super.call(this);
            this.nodeFactory = nodeFactory;
            this.nodeReferences = nodeReferences;
        }
        ComplexNodeFactory.prototype.getNodeFactory = function () {
            return this.nodeFactory;
        };
        ComplexNodeFactory.prototype.getNodeReferences = function () {
            return this.nodeReferences;
        };
        return ComplexNodeFactory;
    })(NodeFactory);
    exports.ComplexNodeFactory = ComplexNodeFactory;
    //#endregion
    var NodeFactoryRegistry = (function (_super) {
        __extends(NodeFactoryRegistry, _super);
        function NodeFactoryRegistry() {
            _super.apply(this, arguments);
            this.factories = {};
        }
        NodeFactoryRegistry.prototype.containsFactoryForType = function (type) {
            return (type in this.factories);
        };
        NodeFactoryRegistry.prototype.getFactoryForType = function (type) {
            return this.factories[type];
        };
        NodeFactoryRegistry.prototype.registerFactory = function (factory) {
            if (this.containsFactoryForType(factory.getAcceptedType()))
                throw "Factory type already registered";
            this.factories[factory.getAcceptedType()] = factory;
        };
        NodeFactoryRegistry.prototype.unregisterFactory = function (factory) {
            if (!this.containsFactoryForType(factory.getAcceptedType()))
                throw "Factory is not yet registered";
            delete this.factories[factory.getAcceptedType()];
        };
        NodeFactoryRegistry.prototype.createFromJson = function (json) {
            if (!this.containsFactoryForType(json["@type"])) {
                throw "Node type '" + json["@type"] + "' not supported";
            }
            return this.getFactoryForType(json["@type"]).createFromJson(json);
        };
        return NodeFactoryRegistry;
    })(NodeFactory);
    exports.NodeFactoryRegistry = NodeFactoryRegistry;
    //#region Simple Factories
    var AbortDepthNodeFactory = (function (_super) {
        __extends(AbortDepthNodeFactory, _super);
        function AbortDepthNodeFactory() {
            _super.apply(this, arguments);
        }
        AbortDepthNodeFactory.prototype.createFromJson = function (json) {
            return new Nodes.AbortDepthNode(json.depth, json.id);
        };
        AbortDepthNodeFactory.prototype.getAcceptedType = function () {
            return "abortDepth";
        };
        return AbortDepthNodeFactory;
    })(NodeFactory);
    exports.AbortDepthNodeFactory = AbortDepthNodeFactory;
    var UnknownNodeFactory = (function (_super) {
        __extends(UnknownNodeFactory, _super);
        function UnknownNodeFactory() {
            _super.apply(this, arguments);
        }
        UnknownNodeFactory.prototype.createFromJson = function (json) {
            return new Nodes.UnknownNode(json.type, json.id);
        };
        UnknownNodeFactory.prototype.getAcceptedType = function () {
            return "unknown";
        };
        return UnknownNodeFactory;
    })(NodeFactory);
    exports.UnknownNodeFactory = UnknownNodeFactory;
    var PrimitiveNodeFactory = (function (_super) {
        __extends(PrimitiveNodeFactory, _super);
        function PrimitiveNodeFactory() {
            _super.apply(this, arguments);
        }
        PrimitiveNodeFactory.prototype.createFromJson = function (json) {
            return new Nodes.PrimitiveNode(json.type, json.value, json.id);
        };
        PrimitiveNodeFactory.prototype.getAcceptedType = function () {
            return "primitive";
        };
        return PrimitiveNodeFactory;
    })(NodeFactory);
    //#endregion
    //#region Complex Factories
    var ReferenceNodeFactory = (function (_super) {
        __extends(ReferenceNodeFactory, _super);
        function ReferenceNodeFactory() {
            _super.apply(this, arguments);
        }
        ReferenceNodeFactory.prototype.createFromJson = function (json) {
            return new Nodes.ReferenceNode(json.referencedId, json.id, this.getNodeReferences());
        };
        ReferenceNodeFactory.prototype.getAcceptedType = function () {
            return "reference";
        };
        return ReferenceNodeFactory;
    })(ComplexNodeFactory);
    exports.ReferenceNodeFactory = ReferenceNodeFactory;
    var ArrayNodeFactory = (function (_super) {
        __extends(ArrayNodeFactory, _super);
        function ArrayNodeFactory() {
            _super.apply(this, arguments);
        }
        ArrayNodeFactory.prototype.createFromJson = function (json) {
            var _this = this;
            var items = json.items.map(function (entry) { return _this.createArrayElementFromJson(entry); });
            return new Nodes.ArrayNode(items, json.id, this.getNodeReferences());
        };
        ArrayNodeFactory.prototype.createArrayElementFromJson = function (json) {
            return new Nodes.ArrayElement(this.getNodeFactory().createFromJson(json.key), this.getNodeFactory().createFromJson(json.value));
        };
        ArrayNodeFactory.prototype.getAcceptedType = function () {
            return "array";
        };
        return ArrayNodeFactory;
    })(ComplexNodeFactory);
    exports.ArrayNodeFactory = ArrayNodeFactory;
    var ObjectNodeFactory = (function (_super) {
        __extends(ObjectNodeFactory, _super);
        function ObjectNodeFactory() {
            _super.apply(this, arguments);
        }
        ObjectNodeFactory.prototype.createFromJson = function (json) {
            var _this = this;
            var properties = json.properties.map(function (prop) { return _this.createObjectPropertyFromJson(prop); });
            return new Nodes.ObjectNode(json.type, properties, json.id, this.getNodeReferences(), json.outline);
        };
        ObjectNodeFactory.prototype.createObjectPropertyFromJson = function (json) {
            return new Nodes.ObjectProperty(json.name, json.isStatic, this.getNodeFactory().createFromJson(json.value), json.visibility, json.declaringClass);
        };
        ObjectNodeFactory.prototype.getAcceptedType = function () {
            return "object";
        };
        return ObjectNodeFactory;
    })(ComplexNodeFactory);
    exports.ObjectNodeFactory = ObjectNodeFactory;
    //#region TraceValue Factories
    var StackTraceFactory = (function (_super) {
        __extends(StackTraceFactory, _super);
        function StackTraceFactory() {
            _super.apply(this, arguments);
        }
        StackTraceFactory.prototype.createFromJson = function (json) {
            var _this = this;
            var callFrames = json.callFrames.map(function (callFrame) {
                return _this.getNodeFactory().createFromJson(callFrame);
            });
            return new Nodes.StackTraceNode(callFrames, json.id, this.getNodeReferences());
        };
        StackTraceFactory.prototype.getAcceptedType = function () {
            return "stackTrace";
        };
        return StackTraceFactory;
    })(ComplexNodeFactory);
    exports.StackTraceFactory = StackTraceFactory;
    var CallFrameFactory = (function (_super) {
        __extends(CallFrameFactory, _super);
        function CallFrameFactory() {
            _super.apply(this, arguments);
        }
        CallFrameFactory.prototype.createFromJson = function (json) {
            var nodeFactory = this.getNodeFactory(), args = nodeFactory.createFromJson(json.content["arguments"]), sourceLine = json.content["sourceLine"], sourceType = json.content["sourceType"], source = null;
            if (json.content.hasOwnProperty("source")) {
                if (json.content["source"]["sourceType"] === "FileSource")
                    source = new Nodes.FileSource(json.content["source"]["path"], json.content["source"]["sourceType"], nodeFactory.createFromJson(json.content["source"]["content"]));
                else
                    source = new Nodes.Source(json.content["source"]["sourceType"], nodeFactory.createFromJson(json.content["source"]["content"]));
            }
            switch (json.type) {
                case "FunctionCallFrame":
                    return new Nodes.FunctionCallFrameNode(json.content["targetFunctionName"], args, sourceLine, source, json.id, this.getNodeReferences());
                case "ClosureCallFrame":
                    var targetClosureScopeObject = json.content["targetClosureScopeObject"] !== null ?
                        nodeFactory.createFromJson(json.content["targetClosureScopeObject"]) : null;
                    var closureHandle = null;
                    if (json.content.hasOwnProperty("targetClosureHandleSource")) {
                        if (json.content["targetClosureHandleSource"]["sourceType"] === "FileSource")
                            closureHandle = new Nodes.ClosureHandle(new Nodes.FileSource(json.content["targetClosureHandleSource"]["path"], json.content["targetClosureHandleSource"]["sourceType"], nodeFactory.createFromJson(json.content["targetClosureHandleSource"]["content"])), json.content["targetClosureHandleContent"]);
                        else
                            closureHandle = new Nodes.ClosureHandle(new Nodes.Source(json.content["targetClosureHandleSource"]["sourceType"], nodeFactory.createFromJson(json.content["targetClosureHandleSource"]["content"])), json.content["targetClosureHandleContent"]);
                    }
                    return new Nodes.ClosureCallFrameNode(json.content["targetClosureScopeNamespace"], json.content["targetClosureScopeClass"], targetClosureScopeObject, closureHandle, json.content["targetFunctionName"], args, sourceLine, source, json.id, this.getNodeReferences());
                case "InstanceMethodCallFrame":
                    return new Nodes.InstanceMethodCallFrameNode(nodeFactory.createFromJson(json.content["targetObject"]), json.content["targetMethodName"], json.content["targetClassName"], args, sourceLine, source, json.id, this.getNodeReferences());
                case "StaticMethodCallFrame":
                    return new Nodes.StaticMethodCallFrameNode(json.content["targetMethodName"], json.content["targetClassName"], args, sourceLine, source, json.id, this.getNodeReferences());
                default:
                    throw "Unknown call type '" + json.type + "'";
            }
        };
        CallFrameFactory.prototype.getAcceptedType = function () {
            return "callFrame";
        };
        return CallFrameFactory;
    })(ComplexNodeFactory);
    exports.CallFrameFactory = CallFrameFactory;
});
//#endregion
//#endregion
//# sourceMappingURL=NodeFactories.js.map