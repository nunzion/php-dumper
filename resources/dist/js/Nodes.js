var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function (require, exports) {
    function getOutlineForNamespacedType(type, cssClass) {
        if (cssClass === void 0) { cssClass = null; }
        var i = type.lastIndexOf("\\");
        if (i >= 0)
            return "<span class=\"" + ((cssClass !== null) ? cssClass : "type") + "\"><span class=\"mouseoverGreyed\">" + type.slice(0, i + 1)
                + "</span>" + type.substr(i + 1) + "</span>";
        else
            return "<span class=\"" + ((cssClass !== null) ? cssClass : "type") + "\">" + type + "</span>";
    }
    function getOutlineForSource(source, sourceLine) {
        if (sourceLine !== null) {
            var result = "<span class=\"calledFrom\"> called from ";
            if (source === null)
                result += "*unknown*";
            else if (source instanceof FileSource) {
                var path = source.getPath();
                var lastSep = Math.max(path.lastIndexOf("\\"), path.lastIndexOf("/"));
                result += "<span class=\"mouseoverGreyed\">" + path.slice(0, lastSep + 1) + "</span>" + path.substr(lastSep + 1);
            }
            else
                result += source.getSourceType().charAt(0).toLocaleLowerCase() + source.getSourceType().slice(1, -6);
            return result + ":" + sourceLine + "</span>";
        }
        else
            return "";
    }
    //#region Abstract Nodes
    var Node = (function () {
        function Node(id) {
            this.id = id;
        }
        Node.prototype.getId = function () {
            return this.id;
        };
        Node.prototype.getType = function () {
            return null;
        };
        Node.prototype.getNodeType = function () {
            throw "Abstract function not implemented";
        };
        Node.prototype.getCriteriaString = function () {
            throw "Abstract function not implemented";
        };
        Node.prototype.getOutline = function () {
            return null;
        };
        Node.prototype.isExpandable = function () {
            return false;
        };
        return Node;
    })();
    exports.Node = Node;
    var ComplexNode = (function (_super) {
        __extends(ComplexNode, _super);
        function ComplexNode(id, nodeReferences) {
            _super.call(this, id);
            this.nodeReferences = nodeReferences;
        }
        ComplexNode.prototype.getElements = function () {
            throw "Abstract function not implemented";
        };
        ComplexNode.prototype.getNodeReferences = function () {
            return this.nodeReferences;
        };
        ComplexNode.prototype.isExpandable = function () {
            return true;
        };
        return ComplexNode;
    })(Node);
    exports.ComplexNode = ComplexNode;
    var CallFrameNode = (function (_super) {
        __extends(CallFrameNode, _super);
        function CallFrameNode(args, sourceLine, source, id, nodeReferences) {
            _super.call(this, id, nodeReferences);
            this.args = args;
            this.sourceLine = sourceLine;
            this.source = source;
        }
        CallFrameNode.prototype.getArgs = function () {
            return this.args;
        };
        CallFrameNode.prototype.getSourceLine = function () {
            return this.sourceLine;
        };
        CallFrameNode.prototype.getSource = function () {
            return this.source;
        };
        CallFrameNode.prototype.getElements = function () {
            return this.args.getElements();
        };
        CallFrameNode.prototype.getCriteriaString = function () {
            var result = "";
            var args = this.args.getElements();
            for (var i = 0; i < args.length; i++)
                result += args[i].getKey().getCriteriaString() + ",";
            return result + ((this.source !== null) ? this.source.getSourceType() : null) + "," + this.sourceLine;
        };
        CallFrameNode.prototype.getArgumentsString = function () {
            var args = this.args.getElements();
            var parts = [];
            for (var i = 0; i < Math.min(3, args.length); i++)
                parts.push(String(args[i].getKey().getOutline(0, false))
                    + ((args[i].getValue().getOutline() !== null) ? " = " + args[i].getValue().getOutline() : ""));
            return "(" + parts.join(", ") + ((args.length > 3) ? ", ..." : "") + ")";
        };
        CallFrameNode.prototype.getOutline = function () {
            throw "CallFrameNodes need an outline";
        };
        return CallFrameNode;
    })(ComplexNode);
    exports.CallFrameNode = CallFrameNode;
    var MethodCallFrameNode = (function (_super) {
        __extends(MethodCallFrameNode, _super);
        function MethodCallFrameNode(targetMethodName, targetClassName, args, sourceLine, source, id, nodeReferences) {
            _super.call(this, args, sourceLine, source, id, nodeReferences);
            this.targetMethodName = targetMethodName;
            this.targetClassName = targetClassName;
        }
        MethodCallFrameNode.prototype.getTargetMethodName = function () {
            return this.targetMethodName;
        };
        MethodCallFrameNode.prototype.getTargetClassName = function () {
            return this.targetClassName;
        };
        MethodCallFrameNode.prototype.getType = function () {
            return "MethodCallFrame";
        };
        MethodCallFrameNode.prototype.getNodeType = function () {
            return "MethodCallFrameNode";
        };
        MethodCallFrameNode.prototype.getCriteriaString = function () {
            return _super.prototype.getCriteriaString.call(this) + this.targetClassName + this.targetMethodName;
        };
        return MethodCallFrameNode;
    })(CallFrameNode);
    exports.MethodCallFrameNode = MethodCallFrameNode;
    //#endregion
    //#region Simple Nodes
    var AbortDepthNode = (function (_super) {
        __extends(AbortDepthNode, _super);
        function AbortDepthNode(depth, id) {
            _super.call(this, id);
            this.depth = depth;
        }
        AbortDepthNode.prototype.getDepth = function () {
            return this.depth;
        };
        AbortDepthNode.prototype.getNodeType = function () {
            return "AbortDepthNode";
        };
        AbortDepthNode.prototype.getCriteriaString = function () {
            return this.depth.toString();
        };
        AbortDepthNode.prototype.getOutline = function () {
            return "**MAX DEPTH**";
        };
        return AbortDepthNode;
    })(Node);
    exports.AbortDepthNode = AbortDepthNode;
    var UnknownNode = (function (_super) {
        __extends(UnknownNode, _super);
        function UnknownNode(type, id) {
            _super.call(this, id);
            this.type = type;
        }
        UnknownNode.prototype.getType = function () {
            return this.type;
        };
        UnknownNode.prototype.getNodeType = function () {
            return "UnknownNode";
        };
        UnknownNode.prototype.getCriteriaString = function () {
            return this.type;
        };
        UnknownNode.prototype.getOutline = function () {
            return "**UNKNOWN**";
        };
        return UnknownNode;
    })(Node);
    exports.UnknownNode = UnknownNode;
    var PrimitiveNode = (function (_super) {
        __extends(PrimitiveNode, _super);
        function PrimitiveNode(type, value, id) {
            _super.call(this, id);
            this.type = type;
            this.value = value;
        }
        PrimitiveNode.prototype.getType = function () {
            return this.type;
        };
        PrimitiveNode.prototype.getValue = function () {
            return this.value;
        };
        PrimitiveNode.prototype.getNodeType = function () {
            return "PrimitiveNode";
        };
        PrimitiveNode.prototype.getCriteriaString = function () {
            return this.type + ":" + String(this.value);
        };
        PrimitiveNode.prototype.getOutline = function (stringTrimOffset, addStringQuotes) {
            if (stringTrimOffset === void 0) { stringTrimOffset = 15; }
            if (addStringQuotes === void 0) { addStringQuotes = true; }
            var result = String(this.value);
            if (this.type === "string") {
                result = result.replace(/\t/g, "    ");
                var trimmed;
                if ((trimmed = (stringTrimOffset > 0) && (result.length > stringTrimOffset + 3)))
                    result = result.substr(0, stringTrimOffset);
                result = result
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/"/g, "\&quot;")
                    .replace(/[^\S\n]/g, "&nbsp;")
                    .replace(/\n/g, "\\n");
                if (trimmed)
                    result += "<span class=\"tripleDot\">...</span>";
                if (addStringQuotes)
                    result = "\"" + result + "\"";
            }
            return "<span class=\"" + this.type + (!addStringQuotes ? " unquoted" : "") + "\">" + result + "</span>";
        };
        PrimitiveNode.prototype.isExpandable = function () {
            return this.type === "string";
        };
        return PrimitiveNode;
    })(Node);
    exports.PrimitiveNode = PrimitiveNode;
    //#endregion
    //#region Complex Nodes
    var ReferenceNode = (function (_super) {
        __extends(ReferenceNode, _super);
        function ReferenceNode(referencedId, id, nodeReferences) {
            _super.call(this, id, nodeReferences);
            this.referencedId = referencedId;
            if (!(referencedId in nodeReferences))
                nodeReferences[referencedId] = null;
        }
        ReferenceNode.prototype.getId = function () {
            var referencedNode = this.getReferencedNode();
            return ((referencedNode !== null) && (referencedNode !== undefined)) ? referencedNode.getId() : this.referencedId;
        };
        ReferenceNode.prototype.getElements = function () {
            var referencedNode = this.getReferencedNode();
            return (referencedNode !== null) ? referencedNode.getElements() : [];
        };
        ReferenceNode.prototype.getReferencedId = function () {
            return this.referencedId;
        };
        ReferenceNode.prototype.getReferencedNode = function () {
            return this.getNodeReferences()[this.referencedId];
        };
        ReferenceNode.prototype.getType = function () {
            var referencedNode = this.getReferencedNode();
            return (referencedNode !== null) ? referencedNode.getType() : null;
        };
        ReferenceNode.prototype.getNodeType = function () {
            return "ReferenceNode";
        };
        ReferenceNode.prototype.getCriteriaString = function () {
            return (this.getReferencedNode() !== null) ? this.getReferencedNode().getCriteriaString() : null;
        };
        ReferenceNode.prototype.getOutline = function () {
            return (this.getReferencedNode() !== null) ? this.getReferencedNode().getOutline() : null;
        };
        ReferenceNode.prototype.isExpandable = function () {
            return (this.getReferencedNode() !== null) ? this.getReferencedNode().isExpandable() : true;
        };
        return ReferenceNode;
    })(ComplexNode);
    exports.ReferenceNode = ReferenceNode;
    var ArrayElement = (function () {
        function ArrayElement(key, value) {
            this.key = key;
            this.value = value;
        }
        ArrayElement.prototype.getKey = function () {
            return this.key;
        };
        ArrayElement.prototype.getValue = function () {
            return this.value;
        };
        return ArrayElement;
    })();
    exports.ArrayElement = ArrayElement;
    var ArrayNode = (function (_super) {
        __extends(ArrayNode, _super);
        function ArrayNode(items, id, nodeReferences, outline) {
            if (outline === void 0) { outline = null; }
            _super.call(this, id, nodeReferences);
            this.items = items;
            this.outline = outline;
        }
        ArrayNode.prototype.getElements = function () {
            return this.items;
        };
        ArrayNode.prototype.getType = function () {
            return "Array";
        };
        ArrayNode.prototype.getNodeType = function () {
            return "ArrayNode";
        };
        ArrayNode.prototype.getCriteriaString = function () {
            return this.items.length.toString();
        };
        ArrayNode.prototype.getOutline = function () {
            if (this.outline === null) {
                var result = "<span class=\"reservedWord\">array</span>(", showElements = true, omitKeys = true;
                for (var i = 0; i < Math.min(3, this.items.length); i++) {
                    showElements = showElements && (this.items[i].getKey().getOutline() !== null) &&
                        (this.items[i].getValue().getOutline() !== null);
                    if (!showElements)
                        break;
                    omitKeys = omitKeys && (this.items[i].getKey().getValue() === i);
                }
                var parts = [];
                if (showElements) {
                    for (var i = 0; i < Math.min(3, this.items.length); i++)
                        parts.push(((!omitKeys) ? this.items[i].getKey().getOutline() + " => " : "")
                            + this.items[i].getValue().getOutline());
                }
                if (parts !== []) {
                    result += parts.join(", ");
                    if (this.items.length > 3)
                        result += ", <span class=\"tripleDot\">...</span>";
                }
                else
                    result += "...";
                return result + ")";
            }
            else
                return this.outline;
        };
        return ArrayNode;
    })(ComplexNode);
    exports.ArrayNode = ArrayNode;
    var ObjectProperty = (function () {
        function ObjectProperty(name, isStatic, value, visibility, declaringClass) {
            this.name = name;
            this.isStatic = isStatic;
            this.value = value;
            this.visibility = visibility;
            this.declaringClass = declaringClass;
        }
        ObjectProperty.prototype.getName = function () {
            return this.name;
        };
        ObjectProperty.prototype.getIsStatic = function () {
            return this.isStatic;
        };
        ObjectProperty.prototype.getValue = function () {
            return this.value;
        };
        ObjectProperty.prototype.getVisibility = function () {
            return this.visibility;
        };
        ObjectProperty.prototype.getDeclaringClass = function () {
            return this.declaringClass;
        };
        return ObjectProperty;
    })();
    exports.ObjectProperty = ObjectProperty;
    var ObjectNode = (function (_super) {
        __extends(ObjectNode, _super);
        function ObjectNode(type, properties, id, nodeReferences, outline) {
            if (outline === void 0) { outline = null; }
            _super.call(this, id, nodeReferences);
            this.type = type;
            this.properties = properties;
            this.outline = outline;
        }
        ObjectNode.prototype.getElements = function () {
            return this.properties;
        };
        ObjectNode.prototype.getType = function () {
            return this.type;
        };
        ObjectNode.prototype.getNodeType = function () {
            return "ObjectNode";
        };
        ObjectNode.prototype.getCriteriaString = function () {
            return this.properties.length.toString();
        };
        ObjectNode.prototype.getOutline = function () {
            if (this.outline === null)
                return getOutlineForNamespacedType(this.type);
            else
                return this.outline;
        };
        return ObjectNode;
    })(ComplexNode);
    exports.ObjectNode = ObjectNode;
    //#region TraceValue Nodes
    var Source = (function () {
        function Source(sourceType, content) {
            this.sourceType = sourceType;
            this.content = content;
        }
        Source.prototype.getSourceType = function () {
            return this.sourceType;
        };
        Source.prototype.getContentNode = function () {
            return this.content;
        };
        return Source;
    })();
    exports.Source = Source;
    var FileSource = (function (_super) {
        __extends(FileSource, _super);
        function FileSource(path, sourceType, content) {
            _super.call(this, sourceType, content);
            this.path = path;
        }
        FileSource.prototype.getPath = function () {
            return this.path;
        };
        return FileSource;
    })(Source);
    exports.FileSource = FileSource;
    var StackTraceNode = (function (_super) {
        __extends(StackTraceNode, _super);
        function StackTraceNode(callFrames, id, nodeReferences, callingLine) {
            if (callingLine === void 0) { callingLine = null; }
            _super.call(this, id, nodeReferences);
            this.callFrames = callFrames;
            this.callingLine = callingLine;
            this.resolved = false;
        }
        StackTraceNode.prototype.resolveCallFrames = function () {
            if (!this.resolved) {
                this.callFrames = this.callFrames.map(function (callFrame) {
                    while (callFrame instanceof ReferenceNode)
                        callFrame = callFrame.getReferencedNode();
                    return callFrame;
                });
                this.resolved = true;
            }
        };
        StackTraceNode.prototype.getMostRecentCallFrame = function () {
            this.resolveCallFrames();
            return this.callFrames[0];
        };
        StackTraceNode.prototype.getLeastRecentCallFrame = function () {
            this.resolveCallFrames();
            return this.callFrames[this.callFrames.length - 1];
        };
        StackTraceNode.prototype.getElements = function () {
            this.resolveCallFrames();
            return this.callFrames;
        };
        StackTraceNode.prototype.getType = function () {
            return "StackTrace";
        };
        StackTraceNode.prototype.getNodeType = function () {
            return "StackTraceNode";
        };
        StackTraceNode.prototype.getCriteriaString = function () {
            return this.callFrames.length.toString();
        };
        StackTraceNode.prototype.getOutline = function () {
            return this.callingLine;
        };
        return StackTraceNode;
    })(ComplexNode);
    exports.StackTraceNode = StackTraceNode;
    var FunctionCallFrameNode = (function (_super) {
        __extends(FunctionCallFrameNode, _super);
        function FunctionCallFrameNode(targetFunctionName, args, sourceLine, source, id, nodeReferences) {
            _super.call(this, args, sourceLine, source, id, nodeReferences);
            this.targetFunctionName = targetFunctionName;
        }
        FunctionCallFrameNode.prototype.getTargetFunctionName = function () {
            return this.targetFunctionName;
        };
        FunctionCallFrameNode.prototype.getType = function () {
            return "FunctionCallFrame";
        };
        FunctionCallFrameNode.prototype.getNodeType = function () {
            return "FunctionCallFrameNode";
        };
        FunctionCallFrameNode.prototype.getCriteriaString = function () {
            return _super.prototype.getCriteriaString.call(this) + this.targetFunctionName;
        };
        FunctionCallFrameNode.prototype.getOutline = function () {
            return getOutlineForNamespacedType(this.targetFunctionName, "function") + this.getArgumentsString()
                + getOutlineForSource(this.source, this.sourceLine);
        };
        return FunctionCallFrameNode;
    })(CallFrameNode);
    exports.FunctionCallFrameNode = FunctionCallFrameNode;
    var ClosureHandle = (function () {
        function ClosureHandle(source, content) {
            this.source = source;
            this.content = content;
        }
        ClosureHandle.prototype.getSource = function () {
            return this.source;
        };
        ClosureHandle.prototype.getContent = function () {
            return this.content;
        };
        return ClosureHandle;
    })();
    exports.ClosureHandle = ClosureHandle;
    var ClosureCallFrameNode = (function (_super) {
        __extends(ClosureCallFrameNode, _super);
        function ClosureCallFrameNode(targetClosureScopeNamespace, targetClosureScopeClass, targetClosureScopeObject, targetClosureHandle, targetFunctionName, args, sourceLine, source, id, nodeReferences) {
            _super.call(this, targetFunctionName, args, sourceLine, source, id, nodeReferences);
            this.targetClosureScopeNamespace = targetClosureScopeNamespace;
            this.targetClosureScopeClass = targetClosureScopeClass;
            this.targetClosureScopeObject = targetClosureScopeObject;
            this.targetClosureHandle = targetClosureHandle;
        }
        ClosureCallFrameNode.prototype.getTargetClosureScopeNamespace = function () {
            return this.targetClosureScopeNamespace;
        };
        ClosureCallFrameNode.prototype.getTargetClosureScopeClass = function () {
            return this.targetClosureScopeClass;
        };
        ClosureCallFrameNode.prototype.getTargetClosureScopeObject = function () {
            return this.targetClosureScopeObject;
        };
        ClosureCallFrameNode.prototype.getTargetClosureHandle = function () {
            return this.targetClosureHandle;
        };
        ClosureCallFrameNode.prototype.isBound = function () {
            return this.targetClosureScopeClass != null;
        };
        ClosureCallFrameNode.prototype.isStatic = function () {
            return this.targetClosureScopeObject == null;
        };
        ClosureCallFrameNode.prototype.getType = function () {
            return "ClosureCallFrame";
        };
        ClosureCallFrameNode.prototype.getNodeType = function () {
            return "ClosureCallFrameNode";
        };
        ClosureCallFrameNode.prototype.getCriteriaString = function () {
            return _super.prototype.getCriteriaString.call(this) + this.targetClosureScopeNamespace + this.targetClosureScopeClass;
        };
        ClosureCallFrameNode.prototype.getOutline = function () {
            var result = "";
            if (this.isBound() && this.isStatic())
                result += getOutlineForNamespacedType(this.targetClosureScopeClass)
                    + "::<span class=\"function\">" + this.targetFunctionName + "</span>";
            else if (this.isBound() && !this.isStatic())
                result += getOutlineForNamespacedType(this.targetClosureScopeClass)
                    + "-><span class=\"function\">" + this.targetFunctionName + "</span>";
            else
                result += "<span class=\"function\">" + this.targetFunctionName + "</span>";
            return result + this.getArgumentsString() + getOutlineForSource(this.source, this.sourceLine);
        };
        return ClosureCallFrameNode;
    })(FunctionCallFrameNode);
    exports.ClosureCallFrameNode = ClosureCallFrameNode;
    var StaticMethodCallFrameNode = (function (_super) {
        __extends(StaticMethodCallFrameNode, _super);
        function StaticMethodCallFrameNode() {
            _super.apply(this, arguments);
        }
        StaticMethodCallFrameNode.prototype.getType = function () {
            return "StaticMethodCallFrame";
        };
        StaticMethodCallFrameNode.prototype.getNodeType = function () {
            return "StaticMethodCallFrameNode";
        };
        StaticMethodCallFrameNode.prototype.getCriteriaString = function () {
            return _super.prototype.getCriteriaString.call(this) + "::";
        };
        StaticMethodCallFrameNode.prototype.getOutline = function () {
            return getOutlineForNamespacedType(this.targetClassName)
                + "::<span class=\"function\">" + this.targetMethodName + "</span>" + this.getArgumentsString()
                + getOutlineForSource(this.source, this.sourceLine);
        };
        return StaticMethodCallFrameNode;
    })(MethodCallFrameNode);
    exports.StaticMethodCallFrameNode = StaticMethodCallFrameNode;
    var InstanceMethodCallFrameNode = (function (_super) {
        __extends(InstanceMethodCallFrameNode, _super);
        function InstanceMethodCallFrameNode(targetObject, targetMethodName, targetClassName, args, sourceLine, source, id, nodeReferences) {
            _super.call(this, targetMethodName, targetClassName, args, sourceLine, source, id, nodeReferences);
            this.targetObject = targetObject;
        }
        InstanceMethodCallFrameNode.prototype.getTargetObject = function () {
            return this.targetObject;
        };
        InstanceMethodCallFrameNode.prototype.getType = function () {
            return "InstanceMethodCallFrame";
        };
        InstanceMethodCallFrameNode.prototype.getNodeType = function () {
            return "InstanceMethodCallFrameNode";
        };
        InstanceMethodCallFrameNode.prototype.getCriteriaString = function () {
            return _super.prototype.getCriteriaString.call(this) + "->";
        };
        InstanceMethodCallFrameNode.prototype.getOutline = function () {
            return getOutlineForNamespacedType(this.targetClassName)
                + "-><span class=\"function\">" + this.targetMethodName + "</span>" + this.getArgumentsString()
                + getOutlineForSource(this.source, this.sourceLine);
        };
        return InstanceMethodCallFrameNode;
    })(MethodCallFrameNode);
    exports.InstanceMethodCallFrameNode = InstanceMethodCallFrameNode;
});
//#endregion
//#endregion
//# sourceMappingURL=Nodes.js.map