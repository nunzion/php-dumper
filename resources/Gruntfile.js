module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-tsd');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bower: {
            install: {}
        },
        tsd: {
            refresh: {
                options: {
                    command: 'reinstall',
                    config: 'tsd.json',
                }
            }
        },
        typescript: {
            ts: {
                src: ['src/ts/**/*.ts'],
                dest: 'dist/js/',
                options: {
                    sourceMap: true,
                    module: 'amd',
                    target: 'es5',
                    rootDir: 'src/ts' // Cut off path
                }
            }
        },
        copy: {
            js: {
                expand: true,
                cwd: 'src/ts',
                src: '**/*.js',
                dest: 'dist/js'
            },
            templates: {
                expand: true,
                cwd: 'src/templates',
                src: '**/*.tmpl.html',
                dest: 'dist/templates'
            }
        },
        sass: {
            sass: {
                options: {
                    unixNewlines: true,
                    style: 'compressed' /* use compact do disable minification */
                },
                expand: true,
                cwd: 'src/sass',
                src: '**/*.scss',
                dest: 'dist/css/',
                ext: '.css'
            }
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: 'dist/js',
                    name: 'startup',
                    paths: {
                        'jquery': '../../lib/jquery/jquery.min'
                    },
                    out: 'dist/js/startup.min.js',
                    include: ["dumper"],
                    keepBuildDir: true,
                    optimize: 'uglify2',
                    gernerateSourceMaps: true,
                    uglify2: {
                        output: {
                            beautify: false
                        },
                        compress: {},
                        warnings: true,
                        mangle: false
                    },
                    optimizeCss: 'none',
                    preserveLicenseComments: false
                }
            }
        },
        watch: {
            ts: {
                files: 'src/ts/**/*.ts',
                tasks: ['typescript:ts']
            },
            js: {
                files: 'src/ts/**/*.js',
                tasks: ['copy:js']
            },
            sass: {
                files: 'src/sass/**/*.scss',
                tasks: ['sass:sass']
            },
            templates: {
                files: 'src/templates/**/*.tmpl.html',
                tasks: ['copy:templates']
            }
        }
    });

    grunt.registerTask('dev', ['bower', 'tsd', 'typescript', 'sass', 'copy', 'watch']);

    grunt.registerTask('default', ['bower', 'tsd', 'typescript', 'sass', 'copy', 'requirejs']);

    grunt.registerTask('offline-dev', ['typescript', 'sass', 'copy', 'watch']);

    grunt.registerTask('offline', ['typescript', 'sass', 'copy', 'requirejs']);
}