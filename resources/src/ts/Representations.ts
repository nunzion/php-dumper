﻿///<reference path="definitions.d.ts"/>

import Nodes = require("Nodes");
import NodeConverters = require("NodeConverters");


export interface RepresentationStateStorage {
    saveCurrentState(): void;
    restoreSavedState(): void;
}

export interface Representation {
    getParentElement(): Element;
    getRepresentationStorage(): RepresentationStateStorage;
}

export interface ComplexRepresentation extends Representation {
    open(id: string|number): Representation;
    close(id: string|number, immidiate?: boolean): boolean;
    switch(id: string|number): void;
    reset(id: string|number): void;
    getState(): { type: String };
    setState(state: { type: String }): void;
}

export interface RootRepresentation extends Representation {
    getNodes(): { [id: string]: Nodes.Node };
    getNodeById(id: string): Nodes.Node;
    getId(): string;
    getState(): { type: String };
    setState(state: { type: String }): void;
}

// Trim every textnode but ignore non-breakable spaces
export function trimHtml(element: Element): void {
    $(element).contents().filter(function () {
        return this.nodeType == Node.TEXT_NODE;
    }).each((i, el) => {
        var newValue = el.nodeValue.replace(/^[^\S\u00A0]+|[^\S\u00A0]+$/g, '');
        if (newValue !== "") el.nodeValue = newValue;
        else el.parentNode.removeChild(el);
    });
}

export class NodeRepresentation<T extends Nodes.Node> implements Representation {
    constructor(protected node: T, protected parentElement: Element, protected representationStorage: RepresentationStateStorage = null) {
    }

    public getNode() {
        return this.node;
    }

    public getParentElement() {
        return this.parentElement;
    }

    public getRepresentationStorage() {
        return this.representationStorage;
    }
}

// FIX for isComplexRepresentaion function
export class ComplexNodeRepresentation<T extends Nodes.ComplexNode> extends NodeRepresentation<T> implements ComplexRepresentation {
    public open(id: string|number): Representation {
        throw "Abstract function not implemented";
    }

    public close(id: string|number, immidiate?: boolean): boolean {
        throw "Abstract function not implemented";
    }

    public switch(id: string|number): void {
        throw "Abstract function not implemented";
    }

    public reset(id: string|number): void {
        throw "Abstract function not implemented";
    }

    public getState(): { type: String } {
        throw "Abstract function not implemented";
    }

    public setState(state: { type: String }): void {
        throw "Abstract function not implemented";
    }
}

export function switchStateClick(event: JQueryEventObject): boolean {
    //var srcElement = $(event.target);
    var data = <{ representation: ComplexRepresentation; id: string|number }>event.data;
    var _this = data.representation;
    _this.switch(data.id);

    if (_this.getRepresentationStorage() !== null)
        _this.getRepresentationStorage().saveCurrentState();
    // TODO scroll to element?
    //$("html, body").scrollTop(srcElement.offset().top);
    return false;
}

export function resetStateClick(event: JQueryEventObject): boolean {
    //var srcElement = $(event.target);
    var data = <{ representation: ComplexRepresentation; id: string|number }>event.data;
    var _this = data.representation;
    _this.reset(data.id);
    _this.open(data.id);

    if (_this.getRepresentationStorage() !== null)
        _this.getRepresentationStorage().saveCurrentState();
    // TODO scroll to element?
    //$("html, body").scrollTop(srcElement.offset().top);
    return false;
}

export function scrollToOriginalEvent(event: JQueryEventObject): boolean {
    $(document).scrollTop(event.data.offset().top);
    if ((<JQuery>event.data)[0].tagName === "DT") {
        var rStart = 98,
            gStart = 147,
            bStart = 211;
        // Finish old animation and set initial css
        (<JQuery>event.data).finish().css("background-color", "rgb(" + rStart + "," + gStart + "," + bStart + ")");
        (<JQuery>event.data).animate({backgroundColor: ""}, {
            duration: 800,
            progress: (animation, progress, remainingMs) => {
                var r = Math.floor((255 - rStart) * progress) + rStart,
                    g = Math.floor((255 - gStart) * progress) + gStart,
                    b = Math.floor((255 - bStart) * progress) + bStart;
                (<JQuery>event.data).css("background-color", "rgb(" + r + "," + g + "," + b + ")");
            },
            always: () => {
                (<JQuery>event.data).css("background-color", ""); // Remove css after animation
            }
        });
    } else console.log("wrong element");
    return false;
};

export function animateShow(element: JQuery, immidiate: boolean = false, finishCallback: () => void = null) {
    element.stop(true, false);
    element.show({
        duration: immidiate ? 0 : 300,
        always: finishCallback,
        complete: () => {
            element.removeAttr("style"); // Remove uncomplete animations
        }
    });
}

export function animateHide(element: JQuery, immidiate: boolean = false, emptyAfterwards: boolean = false, finishCallback: () => void = null) {
    element.stop(true, false);
    element.hide({
        duration: immidiate ? 0 : 300,
        always: () => {
            if (emptyAfterwards)
                element.empty();
            if (finishCallback !== null)
                finishCallback();
        }
    });
}
 