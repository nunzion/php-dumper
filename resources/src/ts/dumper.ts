﻿///<reference path="definitions.d.ts"/>

import $ = require("jquery");
import Template = require("Template");
import Nodes = require("Nodes");
import ObjectHelper = require("ObjectHelper");
import NodeFactories = require("NodeFactories");
import Representations = require("Representations");
import TabRepresentations = require("TabRepresentations");
import NodeConverters = require("NodeConverters");

class SessionStorageRepresentationState implements Representations.RepresentationStateStorage {
    constructor(private rootRepresentation: Representations.RootRepresentation) {
    }

    saveCurrentState(): void {
        var state = this.rootRepresentation.getState();
        sessionStorage.setItem("nunzion-php-dump-" + this.rootRepresentation.getId(), JSON.stringify(state));
    }

    restoreSavedState(): void {
        var session = sessionStorage.getItem("nunzion-php-dump-" + this.rootRepresentation.getId());
        var state: { type: string };
        if ((typeof session !== "undefined") && ((state = JSON.parse(session)) !== null)) {
            this.rootRepresentation.setState(state);
        } else {
            this.rootRepresentation.setState({ type: null });
        }
    }
}

export class JavascriptHtmlDumper implements Representations.RootRepresentation, Representations.ComplexRepresentation {
    public static get showCallingLineOnlyForComplex(): boolean { return true; }

    private nodeRepresentation: Representations.NodeRepresentation<Nodes.Node> = null;
    private nodeParent: JQuery;
    private nodeHeaderParent: JQuery;
    private nodeOpen: boolean = false;
    private nodeNav: JQuery = null;
    private traceRepresentation: Representations.ComplexNodeRepresentation<Nodes.StackTraceNode> = null;
    private traceParent: JQuery;
    private traceHeaderParent: JQuery;
    private traceOpen: boolean = false;
    private traceNav: JQuery = null;
    private representationStorage: Representations.RepresentationStateStorage;

    constructor(private node: Nodes.Node, private nodes: { [id: string]: Nodes.Node }, private trace: Nodes.StackTraceNode, private callingLine: string,
        private parentElement: Element, private id: string,
        representationStorageFactory: (rootRepresentation: Representations.RootRepresentation) => Representations.RepresentationStateStorage) {
        // get and render template
        var tmpl = Template.get("JavascriptHtmlDumper");
        var data = { containsTrace: trace !== null, expandable: node.isExpandable() };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        var elements = $(html);
        parent.append(elements);
        Representations.trimHtml(parentElement);
        // add node and trace to nodes
        if ((nodes[node.getId()] === undefined) || (nodes[node.getId()] === null))
            this.nodes[node.getId()] = node;
        if ((trace !== null) && ((nodes[trace.getId()] === undefined) || (nodes[trace.getId()] === null)))
            this.nodes[trace.getId()] = trace;

        this.nodeHeaderParent = elements.filter("div.javascript-html-header").eq(0);
        Representations.trimHtml(this.nodeHeaderParent[0]);
        this.representationStorage = representationStorageFactory(this);
        this.nodeParent = elements.filter("div.javascript-html-dump").eq(0);
        this.nodeParent.hide();
        // print header
        if ((!JavascriptHtmlDumper.showCallingLineOnlyForComplex || (node instanceof Nodes.ComplexNode)) && (callingLine !== null))
            this.nodeHeaderParent.children("span.outline").html(Template.htmlEncode(callingLine, false));
        else if (node instanceof Nodes.ComplexNode)
            this.nodeHeaderParent.children("span.outline").html((trace === null) ?
                "<span class=\"function\">dump</span>(" + node.getOutline() + ");"
                : trace.getMostRecentCallFrame().getOutline());
        else
            this.nodeHeaderParent.children("span.outline").html(node.getOutline());
        // add possible nav
        if (data.expandable) {
            if (!(node instanceof Nodes.ComplexNode))
                this.nodeParent.addClass("simpleNode");
            this.nodeNav = this.nodeHeaderParent.children("nav").eq(0);
            this.nodeNav.click({ representation: this, id: "node" }, Representations.switchStateClick)
                .dblclick({ representation: this, id: "node" }, Representations.resetStateClick);
        } else
            this.nodeHeaderParent.addClass("simpleNode");
        
        // process trace part
        this.traceHeaderParent = elements.filter("div.javascript-html-called-from-header").eq(0);
        this.traceParent = elements.filter("div.javascript-html-called-from").eq(0);
        this.traceParent.hide();
        if (trace !== null) {
            Representations.trimHtml(this.traceHeaderParent[0]);
            this.traceNav = this.traceHeaderParent.children("nav").eq(0);
            this.traceNav.click({ representation: this, id: "trace" }, Representations.switchStateClick)
                .dblclick({ representation: this, id: "trace" }, Representations.resetStateClick);
            // print header
            var source = trace.getMostRecentCallFrame().getSource();
            var sourceLine = trace.getMostRecentCallFrame().getSourceLine();
            var header = "<span class=\"calledFrom\"> called from ";
            if (source === null)
                header += "*unknown*";
            else if (source instanceof Nodes.FileSource) {
                var path = source.getPath();
                var lastSep = Math.max(path.lastIndexOf("\\"), path.lastIndexOf("/"));
                header += "<span class=\"mouseoverGreyed\">" + path.slice(0, lastSep + 1) + "</span>" + path.substr(lastSep + 1);
            } else
                header += source.getSourceType().charAt(0).toLocaleLowerCase() + source.getSourceType().slice(1, -6);
            if (sourceLine !== null)
                header += ":" + sourceLine;
            header += "</span>";
            this.traceHeaderParent.children("span.outline").html(header);
        }
        // open/close last state
        this.representationStorage.restoreSavedState();
    }

    public getParentElement(): Element {
        return this.parentElement;
    }

    public getRepresentationStorage(): Representations.RepresentationStateStorage {
        return this.representationStorage;
    }

    public getNodes(): { [id: string]: Nodes.Node } {
        return this.nodes;
    }

    public getNodeById(id: string): Nodes.Node {
        return this.nodes[id];
    }

    public getId(): string {
        return this.id;
    }

    public open(id: string|number): Representations.NodeRepresentation<any> {
        if ((id === "node") && this.node.isExpandable()) {
            if (this.nodeRepresentation === null) {
                Representations.animateHide(this.nodeParent, true);
                var elementReferences: { [id: string]: JQuery } = {};
                elementReferences[this.node.getId()] = this.nodeHeaderParent.add(this.nodeParent);
                this.nodeRepresentation = NodeConverters.convertToRepresentation(this.node, this.nodes, this.nodeHeaderParent.add(this.nodeParent),
                    this.nodeParent[0], this.representationStorage, elementReferences);
            }
            Representations.animateShow(this.nodeParent);
            this.nodeOpen = true;
            this.nodeNav.addClass("open");
            return this.nodeRepresentation;
        } else if ((id === "trace") && (this.trace !== null)) {
            if (this.traceRepresentation === null) {
                Representations.animateHide(this.traceParent, true, false,() => { this.traceHeaderParent.removeClass("show"); });
                var elementReferences: { [id: string]: JQuery } = {};
                elementReferences[this.trace.getId()] = this.traceHeaderParent.add(this.traceParent);
                this.traceRepresentation = <Representations.ComplexNodeRepresentation<Nodes.StackTraceNode>>
                NodeConverters.convertToRepresentation(this.trace, this.nodes, this.nodeHeaderParent.add(this.nodeParent),
                    this.traceParent[0], this.representationStorage, elementReferences);
            }
            Representations.animateShow(this.traceParent);
            this.traceHeaderParent.addClass("show");
            this.traceOpen = true;
            this.traceNav.addClass("open");
            return this.traceRepresentation;
        } else
            return null;
    }

    public close(id: string|number, immidiate: boolean = false): boolean {
        if ((id === "node") && this.node.isExpandable()) {
            Representations.animateHide(this.nodeParent);
            this.nodeOpen = false;
            this.nodeNav.removeClass("open");
        } else if ((id === "trace") && (this.trace !== null)) {
            Representations.animateHide(this.traceParent, false, false,() => { this.traceHeaderParent.removeClass("show"); });
            this.traceOpen = false;
            this.traceNav.removeClass("open");
        } else
            return false;
        return true;
    }

    public switch(id: string|number): void {
        var open: boolean;
        if (id === "node")
            open = this.nodeOpen;
        else if (id === "trace")
            open = this.traceOpen;
        else
            return;

        if (open)
            this.close(id);
        else
            this.open(id);
    }

    public reset(id: string|number): void {
        if ((id === "node") && this.node.isExpandable()) {
            Representations.animateHide(this.nodeParent, false, true);
            this.nodeOpen = false;
            this.nodeRepresentation = null;
            this.nodeNav.removeClass("open");
        } else if ((id === "trace") && (this.trace !== null)) {
            Representations.animateHide(this.traceParent, false, true,() => { this.traceHeaderParent.removeClass("show"); });
            this.traceOpen = false;
            this.traceRepresentation = null;
            this.traceNav.removeClass("open");
        }
    }

    public getState(): { type: String; node: any; nodeComplex: boolean; nodeOpen: boolean; trace: any; traceOpen: boolean } {
        var node: any = null,
            nodeComplex: boolean,
            trace: any = null;
        if ((this.nodeRepresentation !== null) && (nodeComplex = TabRepresentations.isComplexRepresentation(this.nodeRepresentation)))
            node = (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).getState();
        else if (this.nodeRepresentation !== null)
            node = this.node.getCriteriaString();
        if (this.traceRepresentation !== null)
            trace = this.traceRepresentation.getState();
        return { type: "JavascriptHtmlDumper", node: node, nodeComplex: nodeComplex, nodeOpen: this.nodeOpen, trace: trace, traceOpen: this.traceOpen };
    }

    public setState(state: { type: String; node: any; nodeComplex: boolean; nodeOpen: boolean; trace: any; traceOpen: boolean }): void {
        if (state.type === "JavascriptHtmlDumper") {
            if (state.node !== null) {
                this.open("node");
                if (!state.nodeOpen)
                    this.close("node", true);
                if (TabRepresentations.isComplexRepresentation(this.nodeRepresentation) && state.nodeComplex) // fits
                    (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).setState(state.node);
                else if (TabRepresentations.isComplexRepresentation(this.nodeRepresentation)) // does not fit
                    (<Representations.ComplexRepresentation>(<any>this.nodeRepresentation)).setState({ type: null });
                else if (this.node.getCriteriaString() !== state.node) // does not fit
                    this.close("node", true);
            } else
                this.reset("node");
            if ((this.trace !== null) && (state.trace !== null)) {
                this.open("trace");
                if (!state.traceOpen)
                    this.close("trace", true);
                this.traceRepresentation.setState(state.trace);
            } else
                this.reset("trace");
        } else {
            this.close("node", true);
            this.reset("node");
            this.close("trace", true);
            this.reset("trace");
        }
    }
}


$(document).ready(function () {
    // Create wait modal
    var modal = $("<div>");
    modal.addClass("nunzion-php-dump-wait-modal");
    modal.css('background-image', 'url(' + require.toUrl('') + "../../images/wait.gif" + ')');
    modal.click(() => {
        modal.remove(); // Allow user to ignore message
    });
    modal.appendTo("body");
    var ready = 0,
        callback = () => {
            ready++;
            if (ready < 2)
                return;
            try {
                $(".nunzion-php-dump").each((idx, element) => {
                    var data = NodeFactories.getFromJson($(element).children(".data").text());
                    $(element).empty();
                    var dumper = new JavascriptHtmlDumper(data.root, data.nodes, data.trace, data.callingLine, element, data.id,
                        (rootRepresentation: Representations.RootRepresentation) => new SessionStorageRepresentationState(rootRepresentation))
                });
            } finally {
                modal.remove();
            }
        }
    Template.loadFromTemplateFile(require.toUrl('') + "../templates/Nodes.tmpl.html", callback);
    Template.loadFromTemplateFile(require.toUrl('') + "../templates/Tabs.tmpl.html", callback);
});
