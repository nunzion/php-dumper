﻿///<reference path="definitions.d.ts"/>

import ObjectHelper = require("ObjectHelper");
import Template = require("Template");
import Nodes = require("Nodes");
import NodeConverters = require("NodeConverters");
import Representations = require("Representations");


export function getTabHeader(node: Nodes.ComplexNode): string[] {
    var result: string[] = [];

    if (node instanceof Nodes.ArrayNode) {
        result = ["Elements (" + node.getElements().length + ")"];
    } else if (node instanceof Nodes.ObjectNode) {
        result = ["Properties (" + node.getElements().length + ")"];
    } else if (node instanceof Nodes.CallFrameNode) {
        result = ["Arguments (" + node.getElements().length + ")"];
        if (node.getSource() !== null)
            result.push(node.getSource().getSourceType().slice(0, -6));
    }
    return result;
}

export function createTab<T extends Nodes.ComplexNode>(tabbedNodeRepresentation: TabbedNodeRepresentation<any>, // TODO pass node
    tabContent: Element, id: string | number): TabRepresentation<T> {
    var result: TabRepresentation<any> = null;
    $(tabContent).empty();
    $(tabContent).hide();

    if (tabbedNodeRepresentation.getNode() instanceof Nodes.ArrayNode) {
        if (id === 0 || id === "elements") {
            result = new ArrayElementsTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    } else if (tabbedNodeRepresentation.getNode() instanceof Nodes.ObjectNode) {
        if (id === 0 || id === "properties") {
            result = new ObjectPropertiesTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    } else if (tabbedNodeRepresentation.getNode() instanceof Nodes.CallFrameNode) {
        if (id === 0 || id === "arguments") {
            result = new ArrayElementsTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        } else if ((tabbedNodeRepresentation.getNode().getSource() !== null) && (id === 1 || id === "source")) {
            result = new SourceTabRepresentation(tabbedNodeRepresentation.getNode(), tabbedNodeRepresentation.getRepresentationStorage(), tabContent,
                tabbedNodeRepresentation.getElementReferences(), tabbedNodeRepresentation.getNodeConverterRegistry());
        }
    }

    return result;
}

// FIX for typescript/javascript not supporting interfaces in instanceof
export function isComplexRepresentation(object: any) {
    return object instanceof TabbedNodeRepresentation
        || object instanceof ComplexTabRepresentation
        || object instanceof Representations.ComplexNodeRepresentation;
}

//#region Abstract Representations

export class TabbedNodeRepresentation<T extends Nodes.ComplexNode> extends Representations.NodeRepresentation<T> implements Representations.ComplexRepresentation {
    protected tabs: TabRepresentation<T>[] = [];
    protected activeTab: number = null;
    protected tabHeader: JQuery[] = [];
    protected tabContents: Element[] = [];
    protected type = "TabbedNode";

    constructor(node: T, parentElement: Element, protected elementReferences: { [id: string]: JQuery },
        protected nodeConverterRegistry: NodeConverters.NodeConverterRegistry, representationStorage: Representations.RepresentationStateStorage = null) {
        super(node, parentElement, representationStorage);
        // get and render template
        var tmpl = Template.get("TabList");
        var tabs: string[] = getTabHeader(node);
        var data = { tabs: tabs };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.hide();
        parent.html(html);
        Representations.trimHtml(parentElement);
        // add content to tabs
        parent.children("ul.header").children("li").each((i, el) => this.tabHeader[i] = $(el));
        parent.children("ul.content").children("li").each((i, el) => this.tabContents[i] = el);
        for (var i = 0; i < tabs.length; i++) {
            this.tabs[i] = createTab<T>(this, this.tabContents[i], i);
            // set onclick & ondblclick
            this.tabHeader[i].click({ representation: this, id: i }, Representations.switchStateClick);
            this.tabHeader[i].dblclick({ representation: this, id: i }, Representations.resetStateClick);
        }
        Representations.animateShow(parent, false,() => {
            Representations.animateShow($(this.tabContents[0]));
        });
        this.tabHeader[0].addClass("active");
        this.activeTab = 0;
    }

    public getTabs(): TabRepresentation<T>[] {
        return this.tabs;
    }

    public getElementReferences(): { [id: string]: JQuery } {
        return this.elementReferences;
    }

    public getNodeConverterRegistry(): NodeConverters.NodeConverterRegistry {
        return this.nodeConverterRegistry;
    }

    public open(id: string|number): TabRepresentation<T> {
        if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined")) {
            if (this.activeTab !== null)
                this.close(this.activeTab, true);
            Representations.animateShow($(this.tabContents[id]));
            this.tabHeader[id].addClass("active");
            this.activeTab = id;
            return this.tabs[id];
        }
        else
            return null;
    }

    public close(id: string|number, immidiate: boolean = false): boolean {
        if ((typeof id === "number") && (typeof this.tabs[id] !== "undefined") && (this.activeTab === id)) {
            Representations.animateHide($(this.tabContents[id]), immidiate, false,
                () => { this.tabHeader[id].removeClass("active"); });
            this.activeTab = null;
            return true;
        }
        else
            return false;
    }

    public switch(id: string|number): void {
        if (typeof id !== "number")
            return;

        if (this.activeTab === id)
            this.close(id);
        else
            this.open(id);
    }

    public reset(id: string|number): void {
        if (typeof id === "number") {
            if (this.activeTab !== null)
                this.close(this.activeTab, true);
            this.tabs[id] = createTab<T>(this, this.tabContents[id], id);
            this.open(id);
        }
    }

    public getState(): { type: string; tabs: any[]; active: any } {
        var result: { type: string; tabs: any[]; active: any } = { type: this.type, tabs: [], active: this.activeTab };
        this.tabs.forEach(function (tab, id) {
            if (isComplexRepresentation(tab)) {
                result.tabs[id] = (<Representations.ComplexRepresentation>(<any>tab)).getState();
            }
        });
        return result;
    }

    public setState(state: { type: string; tabs: any[]; active: any }): void {
        if (state.type !== this.type) {
            this.tabs.forEach((tab, i) => this.reset(i));
            for (var first in this.tabs) break;
            first = +first; // convert to number if possible
            this.open(first);
        } else {
            // change state
            state.tabs.forEach((tab, id) => {
                if ((typeof this.tabs[id] !== "undefined") && isComplexRepresentation(this.tabs[id])) {
                    (<Representations.ComplexRepresentation>(<any>this.tabs[id])).setState(tab);
                }
            });
            if (state.active === null) {
                this.close(this.activeTab, true);
            } else if (!this.open(state.active)) {
                for (var first in this.tabs) break;
                first = +first; // convert to number if possible
                this.open(first);
            }
        }
    }
}

export class TabRepresentation<T extends Nodes.ComplexNode> implements Representations.Representation {
    constructor(protected node: T, protected representationStorage: Representations.RepresentationStateStorage, protected parentElement: Element) {
    }

    public getNode(): T {
        return this.node;
    }

    public getParentElement(): Element {
        return this.parentElement;
    }

    public getRepresentationStorage(): Representations.RepresentationStateStorage {
        return this.representationStorage
    }
}

export class ComplexTabRepresentation<T extends Nodes.ComplexNode> extends TabRepresentation<T> implements Representations.ComplexRepresentation {
    protected elements: {
        title: JQuery; definition: JQuery; nav: JQuery; open: boolean;
        titleRepresentation: Representations.Representation; definitionRepresentation: Representations.Representation; definingNode: Nodes.Node
    }[] = [];
    protected type = "ComplexTab";

    constructor(node: T, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        protected elementReferences: { [id: string]: JQuery }, protected nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement);
    }

    public getElementReferences(): { [id: string]: JQuery } {
        return this.elementReferences;
    }

    public getNodeConverterRegistry(): NodeConverters.NodeConverterRegistry {
        return this.nodeConverterRegistry;
    }

    protected getValue(id: number): Nodes.Node {
        throw "Abstract function not implemented";
    }

    public open(id: string|number): Representations.Representation {
        if ((typeof id !== "number") || (this.elements[id] === undefined) || (this.elements[id].nav === null)) {
            return null;
        } else {
            this.elements[id].nav.addClass("open");
            if (this.elements[id].definitionRepresentation === null) {
                Representations.animateHide(this.elements[id].definition, true, true);
                var value = this.getValue(id);
                var newReferences = ObjectHelper.clone(this.elementReferences);
                newReferences[value.getId()] = this.elements[id].title.add(this.elements[id].definition);
                this.elements[id].definitionRepresentation = this.nodeConverterRegistry.convert(value,
                    this.elements[id].definition[0], newReferences, this.representationStorage);
                this.elements[id].open = false;
            }
            if (!this.elements[id].open) {
                Representations.animateShow(this.elements[id].definition);
                this.elements[id].open = true;
            }
            return this.elements[id].definitionRepresentation;
        }
    }

    public close(id: string|number, immidiate: boolean = false): boolean {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
            return false;
        } else {
            this.elements[id].nav.removeClass("open");
            Representations.animateHide(this.elements[id].definition, immidiate);
            this.elements[id].open = false;
            return true;
        }
    }

    public reset(id: string|number): void {
        if ((typeof id === "number") && (typeof this.elements[id] !== "undefined") && (this.elements[id].nav !== null)) {
            this.elements[id].nav.removeClass("open");
            Representations.animateHide(this.elements[id].definition, false, true);
            this.elements[id].definitionRepresentation = null;
            this.elements[id].open = false;
        }
    }

    public switch(id: string|number): void {
        if ((typeof id !== "number") || (typeof this.elements[id] === "undefined") || (this.elements[id].nav === null)) {
            return;
        } else {
            if (!this.elements[id].open)
                this.open(id);
            else
                this.close(id);
        }
    }

    public getState(): { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] } {
        var elements = this.elements.map((element, id) => {
            if ((element.definitionRepresentation === null) || (element.nav === null))
                return null;
            var node = element.definingNode;
            var state: any = null;
            if ((element.definitionRepresentation !== null) && isComplexRepresentation(element.definitionRepresentation))
                state = (<Representations.ComplexRepresentation>element.definitionRepresentation).getState();
            return { id: id, open: element.open, nodeType: node.getNodeType(), criteria: node.getCriteriaString(), state: state };
        });
        return { type: this.type, elements: elements };
    }

    public setState(state: { type: string; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] }): void {
        if ((state.type !== this.type) || (this.elements.length !== state.elements.length)) {
            this.elements.forEach((element, id) => this.reset(id));
        } else {
            // change state
            this.elements.forEach((element, id) => {
                var node = element.definingNode;
                if ((typeof state.elements[id] !== "undefined") && (state.elements[id] !== null) &&
                    (state.elements[id].nodeType === node.getNodeType()) && (state.elements[id].criteria === node.getCriteriaString())) {
                    this.open(id);
                    if (!state.elements[id].open) this.close(id, true);
                    if ((element.definitionRepresentation !== null) && isComplexRepresentation(element.definitionRepresentation)
                        && (state.elements[id].state !== null))
                        (<Representations.ComplexRepresentation>element.definitionRepresentation).setState(state.elements[id].state);
                } else {
                    this.reset(id);
                }
            });
        }
    }
}

//#endregion

//#region Array TabRepresentations

export class ArrayElementsTabRepresentation extends ComplexTabRepresentation<Nodes.ArrayNode> {
    constructor(node: Nodes.ArrayNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.type = "ArrayElements";
        // TODO categories to filter
        // get and render template
        var tmpl = Template.get("ArrayElements");
        var elementReferences = elementReferences;
        var data = {
            elements: node.getElements(),
            isComplex: (element: Nodes.Node) => element instanceof Nodes.ComplexNode,
            elementReferences: elementReferences
        };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.html(html);
        Representations.trimHtml(parentElement);
        // add converted elements
        var titles = parent.children("dl").children("dt");
        titles.each((i, el) => Representations.trimHtml(el));
        var definitions = parent.children("dl").children("dd");
        definitions.empty();
        for (var key in data.elements) {
            key = +key; // convert to number
            var element = data.elements[key];
            var title = titles.eq(key).children("span.term");
            var definition: JQuery;
            var nav: JQuery = null;
            var titleRepresentation: Representations.Representation;
            var definitionRepresentation: Representations.Representation = null;
            var newReferences = ObjectHelper.clone(elementReferences);
            newReferences[element.getValue().getId()] = titles.eq(key).add(definitions.eq(key));

            // add key content
            titleRepresentation = nodeConverterRegistry.convert(
                element.getKey(), title[0], newReferences, this.getRepresentationStorage());
            if (element.getValue() instanceof Nodes.ComplexNode) {
                // add onclick & ondblclick to nav
                nav = titles.eq(key).children("nav").eq(0);
                nav.click({ representation: this, id: key }, Representations.switchStateClick);
                nav.dblclick({ representation: this, id: key }, Representations.resetStateClick);
                // add onclick to span.id if duplicate
                if (element.getValue().getId() in elementReferences) {
                    var id = titles.eq(key).children("span.id").eq(0);
                    var er = elementReferences[element.getValue().getId()];
                    id.click(er, Representations.scrollToOriginalEvent);
                    id.hover((event) => { er.addClass("markedDuplicate"); },(event) => { er.removeClass("markedDuplicate"); });
                }
                definition = definitions.eq(key);
            } else {
                definition = titles.eq(key).children("span.definition");
                // add value content
                definitionRepresentation = nodeConverterRegistry.convert(
                    element.getValue(), definition[0], newReferences, this.getRepresentationStorage());
            }
            // copy classes to title
            definitions.eq(key).attr('class').split(/\s+/).forEach((className) => titles.eq(key).addClass(className));
            // save element references
            this.elements[key] = {
                title: title, definition: definition, nav: nav, open: !(element.getValue() instanceof Nodes.ComplexNode),
                titleRepresentation: titleRepresentation, definitionRepresentation: definitionRepresentation,
                definingNode: element.getKey()
            };
        }
    }

    protected getValue(id: number): Nodes.Node {
        return this.node.getElements()[id].getValue();
    }
}

//#endregion

//#region Object TabRepresentations

export class ObjectPropertiesTabRepresentation extends ComplexTabRepresentation<Nodes.ObjectNode> {
    protected sortedProperties: Nodes.ObjectProperty[];

    constructor(node: Nodes.ObjectNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.type = "ObjectProperties";
        // TODO categories to filter
        // get and render template
        var tmpl = Template.get("ObjectProperties");
        var elementReferences = elementReferences;
        // sort properties by visibility
        this.sortedProperties = node.getElements().sort((a, b) =>
            ObjectPropertiesTabRepresentation.getPropertyWeight(a) - ObjectPropertiesTabRepresentation.getPropertyWeight(b));
        var data = {
            properties: this.sortedProperties,
            isComplex: (element: Nodes.Node) => element instanceof Nodes.ComplexNode,
            elementReferences: elementReferences,
            declaringClass: node.getType()
        };
        var html = tmpl.render(data);
        // add to parent
        var parent = $(parentElement);
        parent.html(html);
        Representations.trimHtml(parentElement);
        // add converted elements
        var titles = parent.children("dl").children("dt");
        titles.each((i, el) => Representations.trimHtml(el));
        var definitions = parent.children("dl").children("dd");
        definitions.each((i, el) => Representations.trimHtml(el));
        for (var key in data.properties) {
            key = +key; // convert to number
            var property = data.properties[key];
            var definition: JQuery;
            var nav: JQuery = null;
            var definitionRepresentation: Representations.Representation = null;
            var newReferences = ObjectHelper.clone(elementReferences);
            newReferences[property.getValue().getId()] = titles.eq(key).add(definitions.eq(key));

            if (property.getValue() instanceof Nodes.ComplexNode) {
                // add onclick & ondblclick to nav
                nav = titles.eq(key).children("nav").eq(0);
                nav.click({ representation: this, id: key }, Representations.switchStateClick);
                nav.dblclick({ representation: this, id: key }, Representations.resetStateClick);
                // add onclick to span.id if duplicate
                if (property.getValue().getId() in elementReferences) {
                    var id = titles.eq(key).children("span.id").eq(0);
                    var er = elementReferences[property.getValue().getId()];
                    id.click(er, Representations.scrollToOriginalEvent);
                    id.hover((event) => { er.addClass("markedDuplicate"); },(event) => { er.removeClass("markedDuplicate"); });
                }
                definition = definitions.eq(key);
            } else {
                definition = titles.eq(key).children("span.definition");
                titles.eq(key).children("span.term").each((i, el) => Representations.trimHtml(el));
                // add value content
                definitionRepresentation = nodeConverterRegistry.convert(
                    property.getValue(), definition[0], newReferences, this.getRepresentationStorage());
            }
            // copy classes to title
            definitions.eq(key).attr('class').split(/\s+/).forEach((className) => titles.eq(key).addClass(className));
            // save element references
            this.elements[key] = {
                title: titles.eq(key), definition: definition, nav: nav, open: !(property.getValue() instanceof Nodes.ComplexNode),
                titleRepresentation: null, definitionRepresentation: definitionRepresentation,
                definingNode: property.getValue()
            };
        }
    }

    protected static getPropertyWeight(property: Nodes.ObjectProperty): number {
        var result = property.getIsStatic() ? 0 : 100;
        if (property.getVisibility() === "private") result = result + 10;
        if (property.getVisibility() === "protected") result = result + 20;
        if (property.getVisibility() === "public") result = result + 30;
        return result;
    }

    protected getValue(id: number): Nodes.Node {
        return this.sortedProperties[id].getValue();
    }
}

//#endregion

//#region CallFrames TabRepresentations

export class CallFrameArgumentsTabRepresentation<T extends Nodes.CallFrameNode> extends ComplexTabRepresentation<T> {
    private arrayElements: ArrayElementsTabRepresentation;

    constructor(node: T, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
        this.arrayElements = new ArrayElementsTabRepresentation(node.getArgs(), representationStorage, parentElement,
            elementReferences, nodeConverterRegistry);
    }

    public getArrayElementTabRepresentation(): ArrayElementsTabRepresentation {
        return this.arrayElements;
    }

    protected getValue(id: number): Nodes.CallFrameNode {
        throw "Should be unused";
    }

    public open(id: string|number): Representations.Representation {
        throw "Should be unused";
    }

    public close(id: string|number, immidiate?: boolean): boolean {
        throw "Should be unused";
    }

    public reset(id: string|number): void {
        throw "Should be unused";
    }

    public switch(id: string|number): void {
        throw "Should be unused";
    }

    public getState(): { type: string; arrayElements: any; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] } {
        return { type: "CallFrameArguments", arrayElements: this.arrayElements.getState(), elements: [] };
    }

    public setState(state: { type: string; arrayElements: any; elements: { id: number; open: boolean; nodeType: string; criteria: string; state: any }[] }): void {
        if ((state.type === "CallFrameArguments")) {
            this.arrayElements.setState(state.arrayElements);
        } else {
            this.arrayElements.setState({ type: null, elements: [] });
        }
    }
}

export class SourceTabRepresentation extends TabRepresentation<Nodes.CallFrameNode> {
    constructor(node: Nodes.CallFrameNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement);
        if (node.getSource() === null)
            throw "Source cannot be null";
        var parent = $(parentElement);
        parent.empty();
        parent.addClass("source");
        if (node.getSource() instanceof Nodes.FileSource)
            parent.append($("<span>").addClass("path").text((<Nodes.FileSource>node.getSource()).getPath()));
        var content = parent.append("<div>").addClass("content");
        nodeConverterRegistry.convert(node.getSource().getContentNode(), content[0], elementReferences, representationStorage);
    }
}

export class ClosureHandleTabrepresentation extends ComplexTabRepresentation<Nodes.ClosureCallFrameNode> {
    constructor(node: Nodes.ClosureCallFrameNode, representationStorage: Representations.RepresentationStateStorage, parentElement: Element,
        elementReferences: { [id: string]: JQuery }, nodeConverterRegistry: NodeConverters.NodeConverterRegistry) {
        super(node, representationStorage, parentElement, elementReferences, nodeConverterRegistry);
    }
}

//#endregion
