<?php

namespace Nunzion\Debug\Nodes;

use Nunzion\Debug\Chain;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConvertable;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConverter;

class CallFrameNode extends Node implements PlainObjectConvertable
{
    /**
     * @var array
     */
    private $content;

    /**
     * @param string $type
     * @param array  $content
     */
    public function __construct($type, array $content)
    {
        parent::__construct($type);
        $this->content = $content;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        /* @var $first PlainObjectConverter */
        $first = $converterChain->getFirst();
        $content = $this->content;
        $content["arguments"] = $first->convertToPlainObject($content["arguments"], $converterChain);
        if (isset($content["source"]) && ($content["source"] !== null))
        {
            $content["source"]["content"] =
                $first->convertToPlainObject($content["source"]["content"], $converterChain);
            $content["source"] = (object) $content["source"];
        }
        if (isset($content["targetClosureScopeObject"]) && ($content["targetClosureScopeObject"] !== null))
            $content["targetClosureScopeObject"] =
                $first->convertToPlainObject($content["targetClosureScopeObject"], $converterChain);
        if (isset($content["targetObject"]))
            $content["targetObject"] = $first->convertToPlainObject($content["targetObject"], $converterChain);
        if (isset($content["targetClosureHandleSource"]))
        {
            $content["targetClosureHandleSource"]["content"] =
                $first->convertToPlainObject($content["targetClosureHandleSource"]["content"], $converterChain);
            $content["targetClosureHandleSource"] = (object) $content["targetClosureHandleSource"];
        }

        return (object) array(
            "@type"   => "callFrame",
            "id"      => $this->getId(),
            "type"    => $this->getType(),
            "content" => (object) $content
        );
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }
}
