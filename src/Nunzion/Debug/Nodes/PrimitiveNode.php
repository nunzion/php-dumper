<?php

namespace Nunzion\Debug\Nodes;

use Nunzion\Debug\Chain;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConvertable;

class PrimitiveNode extends Node implements PlainObjectConvertable
{
    /**
     * @var mixed
     */
    private $value;

    public function __construct($value)
    {
        parent::__construct(\gettype($value));

        $this->value = $value;
        if (\is_float($value))
        {
            if (\is_nan($value))
                $this->value = "NaN";
            elseif (is_infinite($value))
                $this->value = "Infinite";
        }
        elseif ($value === null)
        {
            $this->value = "null";
        }
    }
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return false;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        return (object) array("@type" => "primitive", "id" => $this->getId(),
                              "value" => $this->value, "type" => $this->getType());
    }
}
