<?php

namespace Nunzion\Debug\Nodes;

use Nunzion\Debug\Chain;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConvertable;

class UnknownNode extends Node implements PlainObjectConvertable
{
    /**
     * @return boolean
     */
    public function isComplex()
    {
        return false;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        return (object) array("@type" => "unknown", "id" => $this->getId(), "type" => $this->getType());
    }
}
