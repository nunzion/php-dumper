<?php

namespace Nunzion\Debug\Nodes;

use Nunzion\Debug\Chain;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConvertable;

class ArrayElement implements PlainObjectConvertable
{
    /**
     * @var Node
     */
    private $key;

    /**
     * @var Node
     */
    private $value;

    public function __construct(Node $key, Node $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return boolean
     */
    public function isComplex()
    {
        return true;
    }

    /**
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Chain $converterChain)
    {
        /* @var $converter PlainObjectConvertable */
        $converter = $converterChain->getFirst();

        return (object) array(
            "key"   => $converter->convertToPlainObject($this->key,
                                                        $converterChain),
            "value" => $converter->convertToPlainObject($this->value,
                                                        $converterChain));
    }
}
