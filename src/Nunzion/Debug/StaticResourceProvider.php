<?php

namespace Nunzion\Debug;

use Exception;

final class StaticResourceProvider
{
    /**
     * @var StaticResourceProvider
     */
    private static $instance = null;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string[][]
     */
    private $resourcePackages = array();

    /**
     * @var string[]
     */
    private $resources = array();

    /**
     * @var boolean[]
     */
    private $providedResources = array();

    private static function notFound($die = true)
    {
        \header("HTTP/1.0 404 Not Found", true, 404);
        if ($die)
            die;
    }

    public static function startup()
    {
        $path = \filter_input(INPUT_SERVER, "PATH_INFO");

        if (\strpos($path, "/nunzion-php-dumper-resource/") !== 0)
            return;

        $requestedRes = \substr($path, strlen("/nunzion-php-dumper-resource"));

        if ($requestedRes !== false)
        {
            $basePath = \realpath(__DIR__ . "/../../../resources/");
            $path = \realpath($basePath . $requestedRes);

            if (strpos($path, $basePath) !== 0) // Forbidden file
                self::notFound();

            if (substr($path, -3) === ".js")
                \header("content-type: text/javascript");
            else if (substr($path, -4) === ".css")
                \header("content-type: text/css");
            else if (substr($path, -4) === ".map")
                \header("content-type: text/json");
            else if (substr($path, -5) === ".html")
                \header("content-type: text/html");
            else if (substr($path, -4) === ".gif")
                \header("content-type: image/gif");

            \readfile($path);
            exit;
        }
        else
            self::notFound();
    }

    /**
     * @return StaticResourceProvider
     */
    public static function getInstance()
    {
        if (self::$instance === null)
            self::$instance = new StaticResourceProvider();

        return self::$instance;
    }

    private function __construct()
    {
        $this->basePath = filter_input(INPUT_SERVER, "SCRIPT_NAME") . "/nunzion-php-dumper-resource";
        $this->resourcePackages["default"] = array("default");
        $this->resources["default"] =
            '<script type="text/javascript">var nunzionPhpDumperBasePath = ' . \json_encode($this->basePath) .
            ';</script>';
        $this->providedResources["default"] = false;
    }

    private function echoResourcePackage($packageName)
    {
        if (!isset($this->resourcePackages[$packageName]))
            throw new Exception("Package '" . $packageName . "' is undefined");

        if (!$this->providedResources[$packageName])
        {
            foreach ($this->resourcePackages[$packageName] as $resourceName)
            {
                if (!$this->providedResources[$resourceName])
                {
                    echo $this->resources[$resourceName];
                    $this->providedResources[$resourceName] = true;
                }
            }
            $this->providedResources[$packageName] = true;
        }
    }

    /**
     * @param string   $packageName
     * @param string[] $css           File name includes for <link>-sections based on the resource folder path
     * @param string[] $js            File name includes for <script>-sections based on the resource folder path
     * @param string[] $preloadImages File name includes for <img>-sections based on the resource folder path(hidden)
     */
    public function registerResources($packageName, array $css = array(),
        array $js = array(), array $preloadImages = array())
    {
        if (isset($this->resourcePackages[$packageName]))
            return;
        $package = array();
        foreach ($css as $file)
        {
            $package[] = $file;
            if (!isset($this->providedResources[$file]))
            {
                $this->resources[$file] =
                    '<link rel="stylesheet" type="text/css" href="' . $this->basePath . '/' . $file . '" />';
                $this->providedResources[$file] = false;
            }
        }
        foreach ($js as $file)
        {
            $package[] = $file;
            if (!isset($this->providedResources[$file]))
            {
                $this->resources[$file] = '<script src="' . $this->basePath . '/' . $file . '"></script>';
                $this->providedResources[$file] = false;
            }
        }
        foreach ($preloadImages as $file)
        {
            $package[] = $file;
            if (!isset($this->providedResources[$file]))
            {
                $this->resources[$file] = '<img src="' . $this->basePath . '/' . $file . '" style="display:none" />';
                $this->providedResources[$file] = false;
            }
        }
        $this->resourcePackages[$packageName] = $package;
        $this->providedResources[$packageName] = false;
    }

    /**
     * @param string $packageName
     */
    public function provideResources($packageName)
    {
        $this->echoResourcePackage("default");
        $this->echoResourcePackage($packageName);
    }
}
