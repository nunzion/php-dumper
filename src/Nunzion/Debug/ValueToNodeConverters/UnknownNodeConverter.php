<?php

namespace Nunzion\Debug\ValueToNodeConverters;

use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\Node;
use Nunzion\Debug\Nodes\UnknownNode;

class UnknownNodeConverter implements ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain)
    {
        return false;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        return new UnknownNode(\gettype($value));
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
    }
}
