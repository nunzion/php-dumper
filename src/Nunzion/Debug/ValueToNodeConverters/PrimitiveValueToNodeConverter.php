<?php

namespace Nunzion\Debug\ValueToNodeConverters;

use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\Node;
use Nunzion\Debug\Nodes\PrimitiveNode;

class PrimitiveValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain)
    {
        if (!\is_scalar($value) && ($value !== null))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->isComplex($value, $converterChain);
        }
        else
            return false;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        if (!\is_scalar($value) && ($value !== null))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->convertToNode($value, $converterChain);
        }

        return new PrimitiveNode($value);
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
    }
}
