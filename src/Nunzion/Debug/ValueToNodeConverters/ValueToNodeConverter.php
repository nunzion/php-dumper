<?php

namespace Nunzion\Debug\ValueToNodeConverters;

use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\Node;

interface ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain);

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    function convertToNode($value, Chain $converterChain);

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain);
}
