<?php

namespace Nunzion\Debug\ValueToNodeConverters;

use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\ArrayElement;
use Nunzion\Debug\Nodes\ArrayNode;
use Nunzion\Debug\Nodes\Node;

class ArrayValueToNodeConverter implements ValueToNodeConverter
{
    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex($value, Chain $converterChain)
    {
        if (!\is_array($value))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->isComplex($value, $converterChain);
        }
        else
            return true;
    }

    /**
     * @param mixed $value
     * @param Chain $converterChain
     * @return Node
     */
    public function convertToNode($value, Chain $converterChain)
    {
        if (!\is_array($value))
        {
            /* @var $next ValueToNodeConverter */
            $next = $converterChain->getNext($this);

            return $next->convertToNode($value, $converterChain);
        }

        /* @var $converter ValueToNodeConverter */
        $converter = $converterChain->getFirst();
        $elements = array();

        foreach ($value as $key => $value)
        {
            $elements[] = new ArrayElement(
                $converter->convertToNode($key, $converterChain),
                $converter->convertToNode($value,
                                          $converterChain));
        }

        return new ArrayNode($elements);
    }

    /**
     * @param Chain $converterChain
     * @return void
     */
    function finish(Chain $converterChain)
    {
        /* @var $next ValueToNodeConverter */
        if (($next = $converterChain->getNext($this)) !== null)
            $next->finish($converterChain);
    }
}
