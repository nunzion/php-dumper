<?php

namespace Nunzion\Debug\Dumpers;

interface Dumper
{
    /**
     * @param mixed $value
     * @param bool  $showStackTrace
     * @return mixed
     */
    function dump($value, $showStackTrace);
}
