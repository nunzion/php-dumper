<?php

namespace Nunzion\Debug\Dumpers;

use Nunzion\Debug\ArrayChain;
use Nunzion\Debug\CurrentDumper;
use Nunzion\Debug\Nodes\JavascriptHtmlDumperNode;
use Nunzion\Debug\PlainObjectConverters\LinearPlainObjectConverter;
use Nunzion\Debug\StaticResourceProvider;
use Nunzion\Debug\ValueToNodeConverters\ValueToNodeConverter;
use Nunzion\StackTrace\CallFrames\FunctionCallFrame;
use Nunzion\StackTrace\EvalSource;
use Nunzion\StackTrace\FileSource;
use Nunzion\StackTrace\Filter;
use Nunzion\StackTrace\StackTrace;
use ReflectionMethod;

class JavascriptHtmlDumper implements Dumper
{
    const USE_MINIFIED_JS = true;

    /**
     * @var StaticResourceProvider
     */
    private $resourceProvider;

    /**
     * @var ArrayChain
     */
    private $valueToNodeConverterChain;

    /**
     * @var ArrayChain
     */
    private $plainObjectConverterChain;

    /**
     * @var string
     */
    private $cssStyle;

    /**
     * @var Filter|null
     */
    private $stackTraceFilter;

    /**
     * @var boolean
     */
    private $printCallingLine;

    /**
     * @var array<string, int>
     */
    private $usedIds = array();

    private function registerResources()
    {
        $startup = "dist/js/startup.js";
        if (self::USE_MINIFIED_JS)
            $startup = "dist/js/startup.min.js";
        $rp = $this->resourceProvider;
        $rp->registerResources("JavascriptHtmlDumper",
                               array("dist/css/" . $this->cssStyle . ".css"),
                               array("lib/requirejs/require.js", $startup),
                               array("images/wait.gif"));
    }

    /**
     * @param ArrayChain                  $valueToNodeConverterChain
     * @param ArrayChain                  $plainObjectConverterChain
     * @param string                      $cssStyle
     * @param StaticResourceProvider|null $resourceProvider
     * @param Filter|null                 $stackTraceFilter
     * @param boolean                     $printCallingLine
     */
    public function __construct(ArrayChain $valueToNodeConverterChain,
        ArrayChain $plainObjectConverterChain,
        $cssStyle,
        StaticResourceProvider $resourceProvider = null,
        Filter $stackTraceFilter = null,
        $printCallingLine = true)
    {
        $this->valueToNodeConverterChain = $valueToNodeConverterChain;
        $this->plainObjectConverterChain = $plainObjectConverterChain;
        $this->cssStyle = $cssStyle;
        $this->resourceProvider = ($resourceProvider !== null) ? $resourceProvider
            : StaticResourceProvider::getInstance();
        $this->registerResources();
        $this->stackTraceFilter = $stackTraceFilter;
        $this->printCallingLine = $printCallingLine;
    }

    public function dump($value, $showCallStack = true)
    {
        // Prevent XDebug cutoff
        \ini_set("xdebug.max_nesting_level", 500);

        /* @var $valueConverter ValueToNodeConverter */
        $valueConverter = $this->valueToNodeConverterChain->getFirst();
        $valueNode = $valueConverter->convertToNode($value, $this->valueToNodeConverterChain);

        $callStack = StackTrace::getCurrent();
        if ($this->stackTraceFilter === null)
            $callStack = CurrentDumper::getStackTraceFilterInstance()->filter($callStack);
        else
            $callStack = $this->stackTraceFilter->filter($callStack);
        $trace = $showCallStack ? $valueConverter->convertToNode($callStack, $this->valueToNodeConverterChain) : null;

        $valueConverter->finish($this->valueToNodeConverterChain); // Complete all pending actions and reset afterwards
        $callingLine = $this->printCallingLine ? $callStack->getMostRecentCallFrame()->getCallingLine() : null;

        $plainObjectConverterChain = new ArrayChain($this->plainObjectConverterChain->getElements());
        $linearPlainObjectConverter = new LinearPlainObjectConverter();
        $plainObjectConverterChain->insertElement($linearPlainObjectConverter);

        $id = $this->getIdFromStackTrace($callStack);
        if (isset($this->usedIds[$id]))
            $id .= ++$this->usedIds[$id];
        else
            $this->usedIds[$id] = 0;

        $dumperNode = new JavascriptHtmlDumperNode($id, $valueNode, $linearPlainObjectConverter, $trace, $callingLine);
        $json = $dumperNode->convertToPlainObject($plainObjectConverterChain);

        $this->resourceProvider->provideResources("JavascriptHtmlDumper");

        echo '<div class="nunzion-php-dump" id="nunzion-php-dump-' . $id . '">';
        echo '<div class="data">';
        echo \htmlspecialchars(\json_encode($json), ENT_NOQUOTES, "UTF-8");
        echo '</div>';
        echo '</div>';
    }

    /**
     * @param StackTrace $trace
     * @return string
     */
    private function getIdFromStackTrace(StackTrace $trace)
    {
        $id = "";
        foreach ($trace->getCallFrames() as $callFrame)
        {
            $reflectionFunction = $callFrame->getTargetReflectionFunction();
            if ($reflectionFunction !== null)
            {
                if ($reflectionFunction instanceof ReflectionMethod)
                    $id .= $reflectionFunction->getDeclaringClass()->getName() . "->";
                $id .= $reflectionFunction->getName() . "(";
                foreach ($reflectionFunction->getParameters() as $parameter)
                    $id .= $parameter->getName() . ",";
                $id = \substr($id, 0, -1) . ")[" . $reflectionFunction->getFileName() .
                      "]"; // file containing function, not file that calls function !!!
            }
            else if ($callFrame instanceof FunctionCallFrame)
            {
                $id .= $callFrame->getTargetFunctionName() . "(";
                foreach ($callFrame->getArgumentsByName() as $name => $arg)
                    $id .= $name . ",";
                $id = \substr($id, 0, -1) . ")[";
                if ($callFrame->hasSource())
                {
                    if ($callFrame->getSource() instanceof FileSource)
                        $id .= $callFrame->getSource()->getPath();
                    else if ($callFrame->getSource() instanceof EvalSource)
                        $id .= "eval";
                }
                $id .= "]";
            }
            $id .= ";";
        }
        $id .= $trace->getMostRecentCallFrame()->getCallingLine(); // TODO getTrimmedCallingLine
        return \md5($id);
    }
}
