<?php

namespace Nunzion\Debug;

interface Chain
{
    function getNext($element);

    function getFirst();
}
