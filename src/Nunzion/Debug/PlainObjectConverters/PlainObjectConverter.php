<?php

namespace Nunzion\Debug\PlainObjectConverters;

use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\Node;

interface PlainObjectConverter
{
    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return boolean
     */
    function isComplex(Node $node, Chain $converterChain);

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return object
     */
    function convertToPlainObject(Node $node, Chain $converterChain);
}
