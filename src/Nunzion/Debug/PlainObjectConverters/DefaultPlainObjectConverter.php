<?php

namespace Nunzion\Debug\PlainObjectConverters;

use Exception;
use Nunzion\Debug\Chain;
use Nunzion\Debug\Nodes\Node;

class DefaultPlainObjectConverter implements PlainObjectConverter
{
    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return boolean
     */
    public function isComplex(Node $node, Chain $converterChain)
    {
        /* @var $node PlainObjectConvertable */
        if ($node instanceof PlainObjectConvertable)
            return $node->isComplex();
        else
            throw new Exception("Not implemented");
    }

    /**
     * @param Node  $node
     * @param Chain $converterChain
     * @return object
     */
    public function convertToPlainObject(Node $node, Chain $converterChain)
    {
        /* @var $node PlainObjectConvertable */
        if ($node instanceof PlainObjectConvertable)
            return $node->convertToPlainObject($converterChain);
        else
            throw new Exception("Not implemented");
    }
}
