<?php

namespace Nunzion\Debug\PlainObjectConverters;

use Nunzion\Debug\Chain;

interface PlainObjectConvertable
{
    /**
     * @return boolean
     */
    function isComplex();

    /**
     * @param Chain $converterChain
     * @return object
     */
    function convertToPlainObject(Chain $converterChain);
}
