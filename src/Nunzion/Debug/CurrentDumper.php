<?php

namespace Nunzion\Debug;

use Exception;
use Nunzion\Debug\Dumpers\Dumper;
use Nunzion\Debug\Dumpers\JavascriptHtmlDumper;
use Nunzion\Debug\PlainObjectConverters\DefaultPlainObjectConverter;
use Nunzion\Debug\PlainObjectConverters\PlainObjectConverter;
use Nunzion\Debug\ValueToNodeConverters\ArrayValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\BreadthFirstSearchValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\DepthFirstSearchValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\ObjectValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\PrimitiveValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\TraceValueToNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\UnknownNodeConverter;
use Nunzion\Debug\ValueToNodeConverters\ValueToNodeConverter;
use Nunzion\StackTrace\Filter;
use Nunzion\StackTrace\FilterCriteria\PathFilterCriterion;

class CurrentDumper
{
    /**
     * @var Dumper|null
     */
    private static $instance = null;

    /**
     * @var Filter|null
     */
    private static $stackTraceFilter = null;

    /**
     * @param Dumper $instance
     */
    public static function setInstance(Dumper $instance)
    {
        self::$instance = $instance;
    }

    /**
     *
     * @return Dumper
     * @throws Exception
     */
    public static function getInstance()
    {
        if (self::$instance === null)
            throw new Exception("Missing instance for Dumper");

        return self::$instance;
    }

    /**
     * @param Filter $filter
     */
    public static function setStackTraceFilter(Filter $filter)
    {
        self::$stackTraceFilter = $filter;
    }

    /**
     * @return Filter
     */
    public static function getNewInternalStackTraceFilterInstance()
    {
        $filter = new Filter();
        // Remove internals, but leave entry frame
        $filter->exclude(new PathFilterCriterion(\realpath(__DIR__ . '\..\..\..')), Filter::AFTER_FIRST, true);

        return $filter;
    }

    /**
     * @return Filter
     */
    public static function getStackTraceFilterInstance()
    {
        if (self::$stackTraceFilter === null)
            self::$stackTraceFilter = self::getNewInternalStackTraceFilterInstance();

        return self::$stackTraceFilter;
    }

    public static function dump($value, $showStackTrace = true)
    {
        self::getInstance()->dump($value, $showStackTrace);
    }

    /**
     * @param ValueToNodeConverter[] $valueToNodeConverters
     * @param PlainObjectConverter[] $plainObjectConverters
     * @param int                    $maxDepth
     * @param bool                   $ignoreDepthIfLastNode Go on for simple nodes even if $maxDepth is exceeded
     * @param boolean                $useBFS                Use breadth-first search instead of depth-first search
     * @param string                 $cssStyle
     */
    public static function init(array $valueToNodeConverters = array(),
                                array $plainObjectConverters = array(),
                                $maxDepth = 15, $ignoreDepthIfLastNode = true,
                                $useBFS = true, $cssStyle = "default")
    {
        // TODO add configuration (+file)
        if (\count($valueToNodeConverters) === 0)
        {
            if ($useBFS)
                $valueToNodeConverters[] = new BreadthFirstSearchValueToNodeConverter($maxDepth,
                                                                                      $ignoreDepthIfLastNode);
            else
                $valueToNodeConverters[] = new DepthFirstSearchValueToNodeConverter($maxDepth,
                                                                                    $ignoreDepthIfLastNode);
            $valueToNodeConverters[] = new PrimitiveValueToNodeConverter();
            $valueToNodeConverters[] = new ArrayValueToNodeConverter();
            $valueToNodeConverters[] = new TraceValueToNodeConverter();
            $valueToNodeConverters[] = new ObjectValueToNodeConverter();
            $valueToNodeConverters[] = new UnknownNodeConverter();
        }
        $valueToNodeConverterChain = new ArrayChain($valueToNodeConverters);

        if (\count($plainObjectConverters) === 0)
        {
            $plainObjectConverters[] = new DefaultPlainObjectConverter();
        }
        $plainObjectConverterChain = new ArrayChain($plainObjectConverters);
        self::setInstance(new JavascriptHtmlDumper($valueToNodeConverterChain,
                                                   $plainObjectConverterChain,
                                                   $cssStyle));
    }
}
